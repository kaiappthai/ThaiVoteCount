![](https://gitlab.com/kaiappthai/ThaiVoteCount/-/raw/master/web/www/votecount/assets/roundedIcon_175.png)

### มานับคะแนนเลือกตั้งด้วยกัน

- แอปนี้ช่วยคุณในการนับคะแนนการเลือกตั้งที่คูหา ทั้งเลือกตั้งเขตและบัญชีรายชื่อ พร้อมทั้งฟังค์ชั่นอัดเสียงขณะขานคะแนน
- ระบบจะบันทึกเวลาทุกการนับคะแนนที่คุณกด สามารถนำมาเทียบกับไฟล์เสียง เพื่อตรวจสอบความถูกต้องได้
- สามารถนำไฟล์เสียงมาเป็นหลักฐานได้
- เมื่อนับเสร็จแล้ว สามารถส่งออกไฟล์บันทึกการนับคะแนนเป็นไฟล์ CSV และเสียงเป็น AAC ไฟล์เสียงขนาดเล็ก (ประมาณ 16 Mb ต่อชั่วโมง)
- สามารถส่งไฟล์ไปที่เซิร์ฟเวอร์ เพื่อเผยแพร่บนหน้าเว็บ (ผู้ใช้จะได้ลิ้งค์หลังจากส่งข้อมูล)

#### ข้อแนะนำการใช้งาน
- สร้างรายงานการโหวต โดยระบุ ประเภทเขต หรือ บัญชีรายชื่อ จังหวัด เขต และหน่วยให้ถูกต้อง ใส่เบอร์ผู้สมัครที่สามารถเลือกได้ให้ถูกต้อง
- ก่อนนับคะแนน กดบันทึกเสียง ปุ่มจะเป็นสีเขียว แล้วเริ่มกดนับตามการขานคะแนน
- เมื่อนับเสร็จแล้ว กดปุ่มบันทึกเสียงอีกครั้งเพื่อปิด ปุ่มจะเป็นสีเทา กดปุ่ม "นับเสร็จแล้ว" พาไปสู่หน้าจัดการข้อมูล
- กดยืนยันการนับคะแนนเสร็จสิ้น คุณจะสามารถ ส่งออกไฟล์นับคะแนน CSV และไฟล์เสียง
- หากต้องการเผยแพร่ข้อมูลบนเว็บ กด ส่งข้อมูลไปเซิร์ฟเวอร์
- ข้อมูลสามารถลบได้ตลอดเวลา คุณสามารถซ้อมใช้งานได้

### โครงสร้าง

- /web เว็บ และ เว็บเซอร์วิส เพื่อรับผลการโหวตและไฟล์เสียง
- /ios แอป iOS (Swift)
- /android แอป Android (Kotlin)
- /flutter แอป iOS / Android / Web App
