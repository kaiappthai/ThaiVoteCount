import 'package:flutter/material.dart';
import 'package:thaivotecount/core/constants/constants.dart';
import 'package:thaivotecount/features/ballot/domain/entities/ballot.dart';
import 'package:thaivotecount/features/ballot/presentation/widgets/nav_back_button.dart';

class BallotCount extends StatefulWidget {
  const BallotCount({ super.key });

  @override
  State<BallotCount> createState() => _BallotCountState();
}

class _BallotCountState extends State<BallotCount> {
  late BallotEntity ballot;
  List num = [];
  List numShow = [];
  int lastNum = 0;
  int isEnd = 0;

  @override
  Widget build(BuildContext context) {
    loadView();
    return Scaffold(
        appBar: _buildAppbar(context),
        body: _buildBody(context)
    );
  }

  _buildAppbar(BuildContext context) {
    return AppBar(
      title: const Text(
        'บันทึกโหวต',
        style: TextStyle(
          fontSize: 17.0,
          fontWeight: FontWeight.w600
        )
      ),
      leadingWidth: 68.0,
      leading: Builder(
        builder: (context) => NavBackButtonWidget(
          tapBackButton: () { _tapBackButton(context); },
        )
      )
    );
  }

  _buildBody(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        _buildHeader(context),

      ]
    );
  }

  _buildHeader(BuildContext context) {
    return Padding(
          padding: const EdgeInsetsDirectional.only(start: 20.0, end: 20.0, bottom: 5.0, top: 5.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                getBallotTitle(),
                style: const TextStyle(
                  fontSize: 16.0,
                ),
              ),
              TextButton(
                onPressed: null,
                style: TextButton.styleFrom(
                  fixedSize: const Size(88, 60),
                  padding: const EdgeInsetsDirectional.only(start: 16.0, end: 16.0, bottom: 8.0, top: 8.0),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.zero,
                    side: BorderSide(width: 1.0, color: Theme.of(context).colorScheme.primary)
                  )
                ),
                child: Text(
                  "นับเสร็จแล้ว ›",
                  style: TextStyle(
                    fontSize: 16.0,
                    fontWeight: FontWeight.bold,
                    color: Theme.of(context).colorScheme.primary
                  )
                ),
              )
            ]
          ),
        );
  }

  void loadView() {
    ballot = ModalRoute.of(context)!.settings.arguments as BallotEntity;
    lastNum = ballot.lastnum ?? 0;
    isEnd = ballot.isEnd ?? 0;
    for (var i = 0; i < (ballot.lastnum ?? 0); i++) {
      num.add([i, 0]);
    }
  }

  String getBallotTitle() {
    return '${getBallotType(ballot.type ?? 0)}\n${ballot.province} เขต ${ballot.region} หน่วย ${ballot.unit}';
  }

  String getBallotType(int type) {
    String ballotType = '';
    for (var item in typeTitle) {
      item.forEach((key, value) {
        if(key == type) {
          ballotType = value;
        }
      });
      if(ballotType != ''){
          break;
      }
    }
    return ballotType;
  }

  void _tapBackButton(BuildContext context) {
    Navigator.pop(context);
  }
}