import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:thaivotecount/core/constants/constants.dart';
import 'package:thaivotecount/features/ballot/domain/entities/ballot.dart';
import 'package:thaivotecount/features/ballot/presentation/bloc/ballot/local/local_ballot_bloc.dart';
import 'package:thaivotecount/features/ballot/presentation/bloc/ballot/local/local_ballot_event.dart';
import 'package:thaivotecount/features/ballot/presentation/widgets/form_dropdown.dart';
import 'package:thaivotecount/features/ballot/presentation/widgets/form_textfield.dart';
import 'package:thaivotecount/features/ballot/presentation/widgets/nav_back_button.dart';
import 'package:thaivotecount/features/ballot/presentation/widgets/submit_button.dart';
import 'package:thaivotecount/injection_container.dart';

class AddBallot extends StatefulWidget {
  const AddBallot({ super.key });

  @override
  State<AddBallot> createState() => _AddBallotState();
}

class _AddBallotState extends State<AddBallot> {
  Map<String, dynamic> newBallot = {
    'name': '',
    'region': 0,
    'unit': 0
  };
  var isSubmit = false;

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => sl<LocalBallotBloc>(),
      child: Scaffold(
        appBar: _buildAppbar(context),
        body: _buildBody(context)
      )
    );
  }

  _buildAppbar(BuildContext context) {
    return AppBar(
      title: const Text(
        'สร้างรายการบันทึก',
        style: TextStyle(
          fontSize: 17.0,
          fontWeight: FontWeight.w600
        )
      ),
      leadingWidth: 68.0,
      leading: Builder(
        builder: (context) => NavBackButtonWidget(
          tapBackButton: () { _tapBackButton(context); },
        )
      )
    );
  }

  _buildBody(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Expanded(
          child: LayoutBuilder(
          builder:
            (BuildContext context, BoxConstraints viewportConstraints) {
          return SingleChildScrollView(
            child: ConstrainedBox(
              constraints: BoxConstraints(
                minHeight: viewportConstraints.maxHeight,
                maxWidth: viewportConstraints.maxWidth,
              ),
              child: Container(
            width: double.infinity,
            padding: const EdgeInsetsDirectional.only(start: 20.0, end: 20.0, bottom: 0.0, top: 12.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                FormTextFieldWidget(
                  data: const {
                    'label': 'ชื่อผู้บันทึก (ไม่จำเป็นต้องระบุ)', 
                    'hint': '',
                  },
                  onChanged: (String string) { newBallot['name'] = string; }
                ),
                FormDropDownWidget(
                  label: 'ประเภท', 
                  options: getTypeOptions(),
                  isExpanded: true,
                  defaultItem: 0,
                  onChanged: (String? string) { newBallot['type'] = int.tryParse(string ?? '0') ?? 0; },
                  textBafore: null,
                ),
                FormDropDownWidget(
                  label: 'จังหวัด', 
                  options: getProvinceOptions(),
                  isExpanded: true,
                  defaultItem: 0,
                  onChanged: (String? string) { newBallot['province'] = string ?? ''; },
                  textBafore: null,
                ),
                FormTextFieldWidget(
                  data: const {
                    'label': 'เขต (ต้องระบุ)', 
                    'hint': 'หมายเลขเขต',
                    'inputType': 'number',
                  },
                  warning: (isSubmit) ? fieldCheck('region') : '',
                  onChanged: (String string) { 
                    setState(() { 
                      newBallot['region'] = int.tryParse(string) ?? 0;
                    }); 
                  },
                ),
                FormTextFieldWidget(
                  data: const {
                    'label': 'หน่วยเลือกตั้ง (ต้องระบุ)', 
                    'hint': 'หมายเลขหน่วยเลือกตั้ง',
                    'inputType': 'number',
                  },
                  warning: (isSubmit) ? fieldCheck('unit') : '',
                  onChanged: (String string) { 
                    setState(() { 
                      newBallot['unit'] = int.tryParse(string) ?? 0; 
                    });
                  },
                ),
                FormDropDownWidget(
                  label: 'เบอร์ผู้สมัครที่เลือกได้', 
                  options: getNumOptions(100),
                  isExpanded: false,
                  defaultItem: 0,
                  onChanged: (String? string) { newBallot['lastnum'] = int.tryParse(string ?? '0') ?? 0; },
                  textBafore: '1 ถึง ',
                ),
              ]
            ),
          )
        ));})),
        SafeArea(
          child: FooterSubmitButtonWidget(
            title: "สร้างรายการบันทึก",
            tapSubmitButton: () {
              _tapAddButton(context);
            }
          ),
        )
      ],
    );
  }

  List<Map<String, String>> getTypeOptions() {
    List<Map<String, String>> options = [];
    for (var item in typeTitle) {
      item.forEach((key, value) {
        Map<String, String> newitem = {
          'value': key.toString(),
          'label': value
        };
        options.add(newitem);
      });
    }
    return options;
  }

  List<Map<String, String>> getProvinceOptions() {
    List<Map<String, String>> options = [];
    for (String item in provinces) {
      Map<String, String> newitem = {
        'value': item,
        'label': item
      };
      options.add(newitem);
    }
    return options;
  }

  List<Map<String, String>> getNumOptions(int lastnum) {
    List<Map<String, String>> options = [];
    for(int i = 2; i <= lastnum; i++) {
      Map<String, String> newitem = {
        'value': i.toString(),
        'label': i.toString()
      };
      options.add(newitem);
    }
    return options;
  }

  String fieldCheck(String type) {
    if (type == 'region' && (newBallot['region'] == null || newBallot['region'] == 0)) {
        return 'กรุณาระบุหมายเลขเขต';
    } else if (type == 'unit' && (newBallot['unit'] == null || newBallot['unit'] == 0)) {
        return 'กรุณาระบุหน่วยเลือกตั้ง';
    }
    return '';
  }

  void _tapBackButton(BuildContext context) {
    Navigator.pop(context);
  }

  void _tapAddButton(BuildContext context) {
    if (newBallot['region'] == null || newBallot['region'] == 0 || newBallot['unit'] == null || newBallot['unit'] == 0) {
      setState(() {
        isSubmit = true;
      });
    } else {
      BallotEntity ballot = BallotEntity(
        id: 0,
        name: newBallot['name'],
        type: newBallot['type'],
        province: newBallot['province'],
        region: newBallot['region'],
        unit: newBallot['unit'],
        lastnum: newBallot['lastnum'],
        isEnd: 0,
        url: '',
        createDate: '',
        endDate: ''
      );
      BlocProvider.of<LocalBallotBloc>(context).add(SaveBallot(ballot));
      Navigator.pop(context);
    }
  }
}