import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:thaivotecount/features/ballot/domain/entities/ballot.dart';
import 'package:thaivotecount/features/ballot/presentation/bloc/ballot/local/local_ballot_bloc.dart';
import 'package:thaivotecount/features/ballot/presentation/bloc/ballot/local/local_ballot_event.dart';
import 'package:thaivotecount/features/ballot/presentation/bloc/ballot/local/local_ballot_state.dart';
import 'package:thaivotecount/features/ballot/presentation/widgets/ballet_none.dart';
import 'package:thaivotecount/features/ballot/presentation/widgets/ballot_tile.dart';
import 'package:thaivotecount/injection_container.dart';

class BallotList extends StatefulWidget {
  const BallotList({ super.key });

  @override
  State<BallotList> createState() => _BallotListState();
}

class _BallotListState extends State<BallotList> {
  LocalBallotBloc ? _bloc;

  @override
  Widget build(BuildContext context) {
    _bloc = sl<LocalBallotBloc>();
    return BlocProvider(
      create: (_) =>_bloc!..add(const GetBallots()),
      child: Scaffold(
        appBar: _buildAppbar(context),
        body: _buildBody(context)
      )
    );
  }

  _buildAppbar(BuildContext context) {
    return AppBar(
      title: const Text(
          'รายการโหวต',
          style: TextStyle(
            fontSize: 17.0,
            fontWeight: FontWeight.w600
          ),
        ),
        actions: [
          IconButton(
            icon: Icon(
              Icons.add, 
              color: Theme.of(context).colorScheme.primary
            ),
            onPressed: () {
              _tapAddButton(context);
            }
          )
        ],
    );
  }

  _buildBody(BuildContext context) {
    return BlocBuilder<LocalBallotBloc, LocalBallotState> (
      builder: (context, state) {
        if (state is LocalBallotNone) {
          return BallotNoneWidget(
            tapAddButton: () => _tapAddButton(context),
          );
        }
        if (state is LocalBallotDone) {
          return Container(
            decoration: const BoxDecoration(
              border: Border(top: BorderSide( width: 1.0, color: Color(0x40808080)))
            ),
            child: ListView.builder(
             itemBuilder: (context, index){
              return BallotWidget(
                ballot: state.ballots![index],
                onBallotPressed: (ballot) => _tapBallot(context, ballot),
              );
             },
             itemCount: state.ballots!.length
            ),
          );
        }
        return const SizedBox();
      },
    );
  }

  void _tapBallot(BuildContext context, BallotEntity ballot) {
    Navigator.pushNamed(context, '/BallotCount', arguments: ballot);
  }

  void _tapAddButton(BuildContext context) {
    Navigator.pushNamed(context, '/AddBallot').then((_) {
      if (_bloc != null) {
        _bloc!.add(const GetBallots());
      }
    });
  }
}