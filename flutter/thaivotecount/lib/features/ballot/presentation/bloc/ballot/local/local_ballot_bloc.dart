import 'package:flutter_bloc/flutter_bloc.dart';
import 'local_ballot_event.dart';
import 'local_ballot_state.dart';
import '../../../../domain/usecases/get_ballots.dart';
import '../../../../domain/usecases/remove_ballot.dart';
import '../../../../domain/usecases/save_ballot.dart';

class LocalBallotBloc extends Bloc<LocalBallotEvent, LocalBallotState> {
  final GetBallotsUseCase _getBallotsUseCase;
  final SaveBallotUseCase _saveBallotUseCase;
  final RemoveBallotUseCase _removeBallotUseCase;

  LocalBallotBloc(
    this._getBallotsUseCase,
    this._saveBallotUseCase,
    this._removeBallotUseCase
  ) : super(const LocalBallotLoading()){
    on <GetBallots> (onGetBallots);
    on <RemoveBallot> (onRemoveBallot);
    on <SaveBallot> (onSaveBallot);
  }


  void onGetBallots(GetBallots event,Emitter<LocalBallotState> emit) async {
    final ballots = await _getBallotsUseCase();
    if (ballots.isNotEmpty) {
      emit(LocalBallotDone(ballots));
    } else {
      emit(const LocalBallotNone());
    }
  }
  
  void onRemoveBallot(RemoveBallot removeBallot,Emitter<LocalBallotState> emit) async {
    await _removeBallotUseCase(params: removeBallot.ballot);
    final ballots = await _getBallotsUseCase();
    emit(LocalBallotDone(ballots));
  }

  void onSaveBallot(SaveBallot saveBallot,Emitter<LocalBallotState> emit) async {
    await _saveBallotUseCase(params: saveBallot.ballot);
    final ballots = await _getBallotsUseCase();
    emit(LocalBallotDone(ballots));
  }
}