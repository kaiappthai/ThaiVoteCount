import 'package:equatable/equatable.dart';
import 'package:thaivotecount/features/ballot/domain/entities/ballot.dart';

abstract class LocalBallotEvent extends Equatable {
  final BallotEntity ? ballot;

  const LocalBallotEvent({this.ballot});

  @override
  List<Object> get props => [ballot!];
}

class GetBallots extends LocalBallotEvent {
  const GetBallots();
}

class RemoveBallot extends LocalBallotEvent {
  const RemoveBallot(BallotEntity ballot) : super(ballot: ballot);
}

class SaveBallot extends LocalBallotEvent {
  const SaveBallot(BallotEntity ballot) : super(ballot: ballot);
}