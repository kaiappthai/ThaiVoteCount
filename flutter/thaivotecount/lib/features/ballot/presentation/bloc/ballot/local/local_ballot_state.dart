import 'package:equatable/equatable.dart';
import 'package:thaivotecount/features/ballot/domain/entities/ballot.dart';

abstract class LocalBallotState extends Equatable {
  final List<BallotEntity> ? ballots;

  const LocalBallotState({this.ballots});

  @override
  List<Object> get props => [ballots!];
}

class LocalBallotLoading extends LocalBallotState {
  const LocalBallotLoading();
}

class LocalBallotDone extends LocalBallotState {
  const LocalBallotDone(List<BallotEntity> ballots) : super(ballots: ballots);
}

class LocalBallotNone extends LocalBallotState {
  const LocalBallotNone();
}