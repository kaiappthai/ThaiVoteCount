import 'package:flutter/material.dart';

class FormDropDownWidget extends StatefulWidget {
  final String ? label;
  final List<Map<String, String>> ? options;
  final bool ? isExpanded;
  final void Function(String?) ? onChanged;
  final int ? defaultItem;
  final String ? textBafore;

  const FormDropDownWidget({
    super.key,
    this.label,
    this.options,
    this.isExpanded,
    this.onChanged,
    this.defaultItem,
    this.textBafore
  });

  @override
  FormDropDownWidgetState createState() => FormDropDownWidgetState();
}

class FormDropDownWidgetState extends State<FormDropDownWidget> {
  String ? selectedVal;

  @override
  void initState() {
    if (widget.defaultItem != null) {
      selectedVal = widget.options?[widget.defaultItem!]['value'];
      widget.onChanged!(widget.options?[widget.defaultItem!]['value']);
    } else {
      selectedVal = widget.options?.first['value'];
      widget.onChanged!(widget.options?.first['value']);
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          widget.label ?? '',
          style: const TextStyle(fontSize: 15.0),
        ),
        const SizedBox(height: 6),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Visibility( 
              visible: (widget.textBafore != null) ? true : false, 
              child: Text(
                widget.textBafore ?? '',
                style: const TextStyle(fontSize: 16.0),
              ),
            ),
            Container(
                height: 40.0,
                width: (widget.isExpanded ?? false) ? MediaQuery.of(context).size.width - 40 : 78.0,
                decoration: BoxDecoration(
                  color: const Color.fromARGB(50, 0, 117, 227),
                  borderRadius: BorderRadius.circular(8),
                ),
                padding: const EdgeInsets.symmetric( vertical: 0.0, horizontal: 12.0),
                child: DropdownButton<String>(
                  isExpanded: widget.isExpanded ?? false,
                  underline: const SizedBox(),
                  items: widget.options?.map<DropdownMenuItem<String>>((Map<String, String> item) {
                    return DropdownMenuItem<String>(
                      value: item['value'],
                      child: Center(
                        child: Text(
                          item['label'] ?? '',
                          textAlign: TextAlign.center,
                          style: TextStyle( 
                            fontSize: 16.0,
                            fontWeight: FontWeight.w600,
                            color: Theme.of(context).colorScheme.primary,
                          ),
                        ),
                      ),
                    );
                  }).toList(),
                  icon: Icon(                // Add this
                    Icons.arrow_drop_down,  // Add this
                    color: Theme.of(context).colorScheme.primary,   // Add this
                  ),
                  value: selectedVal,
                  onChanged: (String? newValue) {
                    if (newValue != null) {
                      setState(() {
                        selectedVal = newValue;
                      });
                      widget.onChanged!(selectedVal ?? '');
                    }
                  },
                ),
            ),
          ],
        ),
        const SizedBox(height: 20)
      ],
    );
  }
}