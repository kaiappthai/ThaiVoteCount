import 'package:flutter/material.dart';

class FooterSubmitButtonWidget extends StatelessWidget {
  final String ? title;
  final void Function() ? tapSubmitButton;

  const FooterSubmitButtonWidget({
    super.key,
    this.title,
    this.tapSubmitButton
  });

  @override
  Widget build(BuildContext context) {
    return Container(
          height: 56.0,
          width: double.infinity,
          margin: const EdgeInsetsDirectional.only(start: 20, end: 20, bottom: 20, top: 8),
          alignment: Alignment.center,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: Theme.of(context).colorScheme.primary,
          ),
          child: TextButton(
            onPressed: () { _tapSubmitButton(); }, 
            child: Text(
              title ?? '',
              style: const TextStyle(
                fontSize: 17.0,
                fontWeight: FontWeight.w600,
                color: Colors.white
              )
            )
          ),
        );
  }

  void _tapSubmitButton() {
    if (tapSubmitButton != null) {
      tapSubmitButton!();
    }
  }
}