import 'package:flutter/material.dart';

class NavBackButtonWidget extends StatelessWidget {
  final void Function() ? tapBackButton;

  const NavBackButtonWidget({
    super.key,
    this.tapBackButton
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () => _tapBackButton(),
      child: Container(
        padding: const EdgeInsetsDirectional.only(start: 8, end: 0, bottom: 0, top: 0),
        alignment: Alignment.centerLeft,
        child: Wrap(
          spacing: -4.0,
          children: [
            Icon(
              Icons.arrow_back_ios, 
              color: Theme.of(context).colorScheme.primary
            ),
            Text ( 
              'กลับ',
              style: TextStyle(
                fontSize: 17.0,
                color: Theme.of(context).colorScheme.primary
              ),
              textAlign: TextAlign.start
            )
          ]
        ),
      )
    );
  }

  void _tapBackButton() {
    if (tapBackButton != null) {
      tapBackButton!();
    }
  }
}