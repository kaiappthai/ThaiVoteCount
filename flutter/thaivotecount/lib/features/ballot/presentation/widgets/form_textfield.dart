import 'package:flutter/material.dart';

class FormTextFieldWidget extends StatefulWidget {
  final Map<String, dynamic> ? data;
  final String ? warning;
  final void Function(String) ? onChanged;

  const FormTextFieldWidget({
    super.key,
    this.data,
    this.warning,
    this.onChanged
  });

  @override
  State<FormTextFieldWidget> createState() => _FormTextFieldWidgetState();
}

class _FormTextFieldWidgetState extends State<FormTextFieldWidget> {
  TextEditingController controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              widget.data?['label'] ?? '',
              style: const TextStyle(fontSize: 15.0),
            ),
            Text(
              widget.warning ?? '',
              textAlign: TextAlign.end,
              style: const TextStyle(
                fontSize: 15.0, 
                color: Colors.red
              ),
            ),
          ],
        ),
        const SizedBox(height: 6),
        SizedBox(
          height: 40.0,
          child: TextField(
            controller: controller,
            onTapOutside: (event) {
              FocusManager.instance.primaryFocus?.unfocus();
            },
            onChanged: onChanged,
            decoration: InputDecoration(
              isDense: true,
              enabledBorder: const OutlineInputBorder(
                borderSide: BorderSide(color: Color(0x80808080), width: 1.0)
              ),
              border: const OutlineInputBorder(
                borderSide: BorderSide(color: Color(0x80808080), width: 1.0)
              ),
              focusedBorder: const OutlineInputBorder(
                borderSide: BorderSide(color: Color(0x80808080), width: 1.0)
              ),
              hintText: widget.data?['hint'] ?? '',
              contentPadding: const EdgeInsets.all(8.0),
              labelStyle: const TextStyle(
                fontSize: 16.0,
              )
            ),
            keyboardType: (widget.data?['inputType'] == 'number') ? const TextInputType.numberWithOptions() : TextInputType.text,
          ),
        ),
        const SizedBox(height: 20)
      ],
    );
  }

  void onChanged(String string) { 
    if (widget.onChanged != null) {
      if (widget.data?['inputType'] == 'number') {
        String str = string.replaceAll(RegExp(r"\D"), "");
        int num = int.tryParse(str) ?? 0;
        String numstr = (num != 0) ? num.toString() : '';
        controller.text = numstr;
        widget.onChanged!(numstr);
      } else {
        widget.onChanged!(string);
      }
    }
  }
}