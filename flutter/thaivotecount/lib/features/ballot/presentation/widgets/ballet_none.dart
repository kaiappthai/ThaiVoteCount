import 'package:flutter/material.dart';

class BallotNoneWidget extends StatelessWidget {
  final void Function() ? tapAddButton;

  const BallotNoneWidget({
    super.key,
    this.tapAddButton
  });

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        padding: const EdgeInsetsDirectional.only(start: 20, end: 20, bottom: 20, top: 20),
        margin: const EdgeInsetsDirectional.only(start: 0, end: 0, bottom: 45, top: 0),
        width: 300.0,
        child: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: _tapAddButton,
          child: RichText(
            text: TextSpan(
              text: 'ยังไม่มีรายการสำหรับบันทึกการโหวต กรุณาคลิก ',
              style: Theme.of(context).textTheme.bodyMedium?.copyWith(fontSize: 19),
              children: <TextSpan>[
                TextSpan(
                  text: '+', 
                  style: TextStyle(
                    fontSize: 24.0,
                    color: Theme.of(context).colorScheme.primary
                  )
                ),
                const TextSpan(text: ' เพื่อสร้างรายการ'),
              ]
            ),
            textAlign: TextAlign.center
          )
        )
      )
    );
  }

  void _tapAddButton() {
    if (tapAddButton != null) {
      tapAddButton!();
    }
  }
}