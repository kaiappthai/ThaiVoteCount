import 'package:flutter/material.dart';
import 'package:thaivotecount/features/ballot/domain/entities/ballot.dart';

import '../../../../core/constants/constants.dart';

class BallotWidget extends StatelessWidget {
  final BallotEntity ? ballot;
  final void Function(BallotEntity ballot) ? onBallotPressed;

  const BallotWidget({
    super.key,
    this.ballot,
    this.onBallotPressed
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: _onTap,
      child: Container(
        decoration: const BoxDecoration(
          border: Border(bottom: BorderSide( width: 1.0, color: Color(0x40808080)))
        ),
        padding: const EdgeInsetsDirectional.only(start: 20, end: 20, bottom: 0, top: 0),
          height: 90.0,
          child: Row(
            children: [
              _buildTitle(context)
            ],
          ),
      ),
    );
  }

  Widget _buildTitle(BuildContext context) {
    return Expanded(
      child: Row(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          // Title
          Expanded(
            child: Text(
              getBallotTitle(),
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                fontSize: 17,
                color: Theme.of(context).colorScheme.onPrimary,
              ),
            )
          ),
          const Opacity(
            opacity: 0.38,
            child: Icon(
              Icons.chevron_right_sharp,
            ),
          )
        ],
      ),
    );
  }

  String getBallotTitle() {
    return '${getBallotType(ballot!.type ?? 0)}\n${ballot!.province} เขต ${ballot!.region} หน่วย ${ballot!.unit}';
  }

  String getBallotType(int type) {
    String ballotType = '';
    for (var item in typeTitle) {
      item.forEach((key, value) {
        if(key == type) {
          ballotType = value;
        }
      });
      if(ballotType != ''){
          break;
      }
    }
    return ballotType;
  }

  void _onTap() {
    if (onBallotPressed != null) {
      onBallotPressed!(ballot!);
    }
  }
}