part of 'app_database.dart';

class $FloorAppDatabase {
  // ignore: library_private_types_in_public_api
  static _$AppDatabaseBuilder databaseBuilder(String name) =>
      _$AppDatabaseBuilder(name);
  // ignore: library_private_types_in_public_api
  static _$AppDatabaseBuilder inMemoryDatabaseBuilder() =>
      _$AppDatabaseBuilder(null);
}

class _$AppDatabaseBuilder {
  _$AppDatabaseBuilder(this.name);

  final String? name;

  final List<Migration> _migrations = [];

  Callback? _callback;

  /// Adds migrations to the builder.
  _$AppDatabaseBuilder addMigrations(List<Migration> migrations) {
    _migrations.addAll(migrations);
    return this;
  }

  /// Adds a database [Callback] to the builder.
  _$AppDatabaseBuilder addCallback(Callback callback) {
    _callback = callback;
    return this;
  }

  /// Creates the database and initializes it.
  Future<AppDatabase> build() async {
    final path = name != null
        ? await sqfliteDatabaseFactory.getDatabasePath(name!)
        : ':memory:';
    final database = _$AppDatabase();
    database.database = await database.open(
      path,
      _migrations,
      _callback,
    );
    return database;
  }
}

class _$AppDatabase extends AppDatabase {
  _$AppDatabase([StreamController<String>? listener]) {
    changeListener = listener ?? StreamController<String>.broadcast();
  }

  BallotDao? _ballotDAOInstance;

  Future<sqflite.Database> open(String path, List<Migration> migrations,
      [Callback? callback]) async {
    final databaseOptions = sqflite.OpenDatabaseOptions(
      version: 1,
      onConfigure: (database) async {
        await database.execute('PRAGMA foreign_keys = ON');
        await callback?.onConfigure?.call(database);
      },
      onOpen: (database) async {
        await callback?.onOpen?.call(database);
      },
      onUpgrade: (database, startVersion, endVersion) async {
        await MigrationAdapter.runMigrations(
            database, startVersion, endVersion, migrations);

        await callback?.onUpgrade?.call(database, startVersion, endVersion);
      },
      onCreate: (database, version) async {
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `ballot` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `name` TEXT, `type` INTEGER, `province` TEXT, `region` INTEGER, `unit` INTEGER, `lastnum` INTEGER, `isEnd` INTEGER, `url` TEXT, `createDate` TEXT, `endDate` TEXT)');

        await callback?.onCreate?.call(database, version);
      },
    );
    return sqfliteDatabaseFactory.openDatabase(path, options: databaseOptions);
  }

  @override
  BallotDao get ballotDAO {
    return _ballotDAOInstance ??= _$BallotDao(database, changeListener);
  }
}

class _$BallotDao extends BallotDao {
  _$BallotDao(this.database, this.changeListener)
      : _queryAdapter = QueryAdapter(database),
        _ballotModelInsertionAdapter = InsertionAdapter(
            database,
            'ballot',
            (BallotModel item) => <String, Object?>{
                  'name': item.name,
                  'type': item.type,
                  'province': item.province,
                  'region': item.region,
                  'unit': item.unit,
                  'lastnum': item.lastnum,
                  'isEnd': item.isEnd,
                  'url': item.url,
                  'createDate': item.createDate,
                  'endDate': item.endDate
                }),
        _ballotModelDeletionAdapter = DeletionAdapter(
            database,
            'ballot',
            ['id'],
            (BallotModel item) => <String, Object?>{
                  'id': item.id,
                  'name': item.name,
                  'type': item.type,
                  'province': item.province,
                  'region': item.region,
                  'unit': item.unit,
                  'lastnum': item.lastnum,
                  'isEnd': item.isEnd,
                  'url': item.url,
                  'createDate': item.createDate,
                  'endDate': item.endDate
                });

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  final InsertionAdapter<BallotModel> _ballotModelInsertionAdapter;

  final DeletionAdapter<BallotModel> _ballotModelDeletionAdapter;

  @override
  Future<List<BallotModel>> getBallots() async {
    return _queryAdapter.queryList('SELECT * FROM ballot',
      mapper: (Map<String, Object?> row) => BallotModel(
        id: row['id'] as int?,
        name: row['name'] as String?,
        type: row['type'] as int?,
        province: row['province'] as String?,
        region: row['region'] as int?,
        unit: row['unit'] as int?,
        lastnum: row['lastnum'] as int?,
        isEnd: row['isEnd'] as int?,
        url: row['url'] as String?,
        createDate: row['createDate'] as String?,
        endDate: row['endDate'] as String?
      )
    );
  }

  @override
  Future<void> insertBallot(BallotModel ballot) async {
    await _ballotModelInsertionAdapter.insert(
        ballot, OnConflictStrategy.abort);
  }

  @override
  Future<void> deleteBallot(BallotModel ballotModel) async {
    await _ballotModelDeletionAdapter.delete(ballotModel);
  }
}