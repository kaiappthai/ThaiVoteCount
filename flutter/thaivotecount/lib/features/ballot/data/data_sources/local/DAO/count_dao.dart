import 'package:floor/floor.dart';
import 'package:thaivotecount/features/ballot/data/models/count.dart';

@dao
abstract class CountDao {
  
  @Insert()
  Future<void> insertCount(CountModel article);
  
  @Query('SELECT * FROM count')
  Future<List<CountModel>> getCounts(int ballotID);
}