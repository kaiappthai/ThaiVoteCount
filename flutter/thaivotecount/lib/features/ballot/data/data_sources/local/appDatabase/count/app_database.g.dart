part of 'app_database.dart';

class $FloorAppDatabase {
  // ignore: library_private_types_in_public_api
  static _$AppDatabaseBuilder databaseBuilder(String name) =>
      _$AppDatabaseBuilder(name);
  // ignore: library_private_types_in_public_api
  static _$AppDatabaseBuilder inMemoryDatabaseBuilder() =>
      _$AppDatabaseBuilder(null);
}

class _$AppDatabaseBuilder {
  _$AppDatabaseBuilder(this.name);

  final String? name;

  final List<Migration> _migrations = [];

  Callback? _callback;

  /// Adds migrations to the builder.
  _$AppDatabaseBuilder addMigrations(List<Migration> migrations) {
    _migrations.addAll(migrations);
    return this;
  }

  /// Adds a database [Callback] to the builder.
  _$AppDatabaseBuilder addCallback(Callback callback) {
    _callback = callback;
    return this;
  }

  /// Creates the database and initializes it.
  Future<AppDatabase> build() async {
    final path = name != null
        ? await sqfliteDatabaseFactory.getDatabasePath(name!)
        : ':memory:';
    final database = _$AppDatabase();
    database.database = await database.open(
      path,
      _migrations,
      _callback,
    );
    return database;
  }
}

class _$AppDatabase extends AppDatabase {
  _$AppDatabase([StreamController<String>? listener]) {
    changeListener = listener ?? StreamController<String>.broadcast();
  }

  CountDao? _countDAOInstance;

  Future<sqflite.Database> open(String path, List<Migration> migrations,
      [Callback? callback]) async {
    final databaseOptions = sqflite.OpenDatabaseOptions(
      version: 1,
      onConfigure: (database) async {
        await database.execute('PRAGMA foreign_keys = ON');
        await callback?.onConfigure?.call(database);
      },
      onOpen: (database) async {
        await callback?.onOpen?.call(database);
      },
      onUpgrade: (database, startVersion, endVersion) async {
        await MigrationAdapter.runMigrations(
            database, startVersion, endVersion, migrations);

        await callback?.onUpgrade?.call(database, startVersion, endVersion);
      },
      onCreate: (database, version) async {
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `count` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `vnum` INTEGER, `vadd` INTEGER, `vdate` TEXT)');

        await callback?.onCreate?.call(database, version);
      },
    );
    return sqfliteDatabaseFactory.openDatabase(path, options: databaseOptions);
  }

  @override
  CountDao get countDAO {
    return _countDAOInstance ??= _$CountDao(database, changeListener);
  }
}

class _$CountDao extends CountDao {
  _$CountDao(this.database, this.changeListener)
      : _queryAdapter = QueryAdapter(database),
        _countModelInsertionAdapter = InsertionAdapter(
            database,
            'ballot',
            (CountModel item) => <String, Object?>{
                  'vnum': item.vnum,
                  'vadd': item.vadd,
                  'vdate': item.vdate
                });

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  final InsertionAdapter<CountModel> _countModelInsertionAdapter;

  @override
  Future<List<CountModel>> getCounts(int ballotID) async {
    return _queryAdapter.queryList('SELECT * FROM count',
      mapper: (Map<String, Object?> row) => CountModel(
        id: row['id'] as int?,
        vnum: row['vnum'] as int?,
        vadd: row['vadd'] as int?,
        vdate: row['vdate'] as String?
      )
    );
  }

  @override
  Future<void> insertCount(CountModel count) async {
    await _countModelInsertionAdapter.insert(
        count, OnConflictStrategy.abort);
  }
}