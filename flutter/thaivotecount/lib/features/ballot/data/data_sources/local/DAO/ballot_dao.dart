import 'package:floor/floor.dart';
import 'package:thaivotecount/features/ballot/data/models/ballot.dart';

@dao
abstract class BallotDao {
  
  @Insert()
  Future<void> insertBallot(BallotModel article);
  
  @delete
  Future<void> deleteBallot(BallotModel articleModel);
  
  @Query('SELECT * FROM ballot')
  Future<List<BallotModel>> getBallots();
}