import 'package:floor/floor.dart';
import 'package:thaivotecount/features/ballot/data/data_sources/local/DAO/count_dao.dart';
import 'package:thaivotecount/features/ballot/data/models/count.dart';
import 'package:sqflite/sqflite.dart' as sqflite;
import 'dart:async';

part 'app_database.g.dart';

@Database(version: 1, entities: [CountModel])
abstract class AppDatabase extends FloorDatabase {
  CountDao get countDAO;
}