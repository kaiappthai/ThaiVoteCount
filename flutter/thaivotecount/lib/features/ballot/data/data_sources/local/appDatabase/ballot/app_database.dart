import 'package:floor/floor.dart';
import 'package:thaivotecount/features/ballot/data/data_sources/local/DAO/ballot_dao.dart';
import 'package:thaivotecount/features/ballot/data/models/ballot.dart';
import 'package:sqflite/sqflite.dart' as sqflite;
import 'dart:async';

part 'app_database.g.dart';

@Database(version: 1, entities: [BallotModel])
abstract class AppDatabase extends FloorDatabase {
  BallotDao get ballotDAO;
}