import 'package:thaivotecount/features/ballot/data/data_sources/local/appDatabase/ballot/app_database.dart';
import 'package:thaivotecount/features/ballot/data/models/ballot.dart';
import 'package:thaivotecount/features/ballot/domain/entities/ballot.dart';
import 'package:thaivotecount/features/ballot/domain/repository/ballot_repository.dart';

class BallotRepositoryImpl implements BallotRepository {
  final AppDatabase _appDatabase;
  BallotRepositoryImpl(this._appDatabase);

  @override
  Future<List<BallotModel>> getBallots() async {
    return _appDatabase.ballotDAO.getBallots();
  }

  @override
  Future<void> removeBallot(BallotEntity ballot) {
    return _appDatabase.ballotDAO.deleteBallot(BallotModel.fromEntity(ballot));
  }

  @override
  Future<void> saveBallot(BallotEntity ballot) {
    return _appDatabase.ballotDAO.insertBallot(BallotModel.fromEntity(ballot));
  }
  
}