import 'package:thaivotecount/features/ballot/data/data_sources/local/appDatabase/count/app_database.dart';
import 'package:thaivotecount/features/ballot/data/models/count.dart';
import 'package:thaivotecount/features/ballot/domain/entities/count.dart';
import 'package:thaivotecount/features/ballot/domain/repository/count_repository.dart';

class CountRepositoryImpl implements CountRepository {
  final AppDatabase _appDatabase;
  CountRepositoryImpl(this._appDatabase);

  @override
  Future<List<CountModel>> getCounts(int ballotID) async {
    return _appDatabase.countDAO.getCounts(ballotID);
  }

  @override
  Future<void> saveCount(CountEntity count) {
    return _appDatabase.countDAO.insertCount(CountModel.fromEntity(count));
  }
  
}