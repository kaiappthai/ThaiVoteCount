import 'package:floor/floor.dart';
import 'package:thaivotecount/features/ballot/domain/entities/count.dart';

@Entity(tableName: 'count',primaryKeys: ['id'])
class CountModel extends CountEntity {
  const CountModel({
    super.id,
    super.vnum,
    super.vadd,
    super.vdate
  });

  factory CountModel.fromEntity(CountEntity entity) {
    return CountModel(
      id: entity.id,
      vnum: entity.vnum,
      vadd: entity.vadd,
      vdate: entity.vdate
    );
  }
}