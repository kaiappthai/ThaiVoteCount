import 'package:floor/floor.dart';
import 'package:thaivotecount/features/ballot/domain/entities/ballot.dart';

@Entity(tableName: 'ballot',primaryKeys: ['id'])
class BallotModel extends BallotEntity {
  const BallotModel({
    super.id,
    super.name,
    super.type,
    super.province,
    super.region,
    super.unit,
    super.lastnum,
    super.isEnd,
    super.url,
    super.createDate,
    super.endDate
  });

  factory BallotModel.fromEntity(BallotEntity entity) {
    return BallotModel(
      id: entity.id,
      name: entity.name,
      type: entity.type,
      province: entity.province,
      region: entity.region,
      unit: entity.unit,
      lastnum: entity.lastnum,
      isEnd: entity.isEnd,
      url: entity.url,
      createDate: entity.createDate,
      endDate: entity.endDate
    );
  }
}