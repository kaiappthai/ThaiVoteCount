import 'package:equatable/equatable.dart';

class BallotEntity extends Equatable{
  final int ? id;
  final String ? name;
  final int ? type;
  final String ? province;
  final int ? region;
  final int ? unit;
  final int ? lastnum;
  final int ? isEnd;
  final String ? url;
  final String ? createDate;
  final String ? endDate;

  const BallotEntity({
    this.id,
    this.name,
    this.type,
    this.province,
    this.region,
    this.unit,
    this.lastnum,
    this.isEnd,
    this.url,
    this.createDate,
    this.endDate,
  });

  @override
  List < Object ? > get props {
    return [
      id,
      name,
      type,
      province,
      region,
      unit,
      lastnum,
      isEnd,
      url,
      createDate,
      endDate,
    ];
  }
}