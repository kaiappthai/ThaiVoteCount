import 'package:equatable/equatable.dart';

class CountEntity extends Equatable{
  final int ? id;
  final int ? vnum;
  final int ? vadd;
  final String ? vdate;

  const CountEntity({
    this.id,
    this.vnum,
    this.vadd,
    this.vdate,
  });

  @override
  List < Object ? > get props {
    return [
      id,
      vnum,
      vadd,
      vdate,
    ];
  }
}