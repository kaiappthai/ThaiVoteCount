import 'package:thaivotecount/core/usecases/usecase.dart';
import 'package:thaivotecount/features/ballot/domain/entities/ballot.dart';
import 'package:thaivotecount/features/ballot/domain/repository/ballot_repository.dart';

class GetBallotsUseCase implements UseCase<List<BallotEntity>,void>{
  
  final BallotRepository _ballotRepository;

  GetBallotsUseCase(this._ballotRepository);
  
  @override
  Future<List<BallotEntity>> call({void params}) {
    return _ballotRepository.getBallots();
  }
  
}