import 'package:thaivotecount/core/usecases/usecase.dart';
import 'package:thaivotecount/features/ballot/domain/entities/count.dart';
import 'package:thaivotecount/features/ballot/domain/repository/count_repository.dart';

class SaveCountUseCase implements UseCase<void, CountEntity> {
  
  final CountRepository _countRepository;

  SaveCountUseCase(this._countRepository);
  
  @override
  Future<void> call({CountEntity ? params}) {
    return _countRepository.saveCount(params!);
  }
  
}