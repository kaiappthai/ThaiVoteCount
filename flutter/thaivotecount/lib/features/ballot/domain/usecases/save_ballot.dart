import 'package:thaivotecount/core/usecases/usecase.dart';
import 'package:thaivotecount/features/ballot/domain/entities/ballot.dart';
import 'package:thaivotecount/features/ballot/domain/repository/ballot_repository.dart';

class SaveBallotUseCase implements UseCase<void, BallotEntity> {
  
  final BallotRepository _ballotRepository;

  SaveBallotUseCase(this._ballotRepository);
  
  @override
  Future<void> call({BallotEntity ? params}) {
    return _ballotRepository.saveBallot(params!);
  }
  
}