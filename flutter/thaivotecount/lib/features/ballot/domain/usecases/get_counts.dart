import 'package:thaivotecount/core/usecases/usecase.dart';
import 'package:thaivotecount/features/ballot/domain/entities/count.dart';
import 'package:thaivotecount/features/ballot/domain/repository/count_repository.dart';

class GetCountUseCase implements UseCase<List<CountEntity>,int>{
  
  final CountRepository _countRepository;

  GetCountUseCase(this._countRepository);
  
  @override
  Future<List<CountEntity>> call({int ? params}) {
    return _countRepository.getCounts(params ?? 0);
  }
  
}