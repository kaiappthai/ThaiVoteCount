import 'package:thaivotecount/features/ballot/domain/entities/ballot.dart';

abstract class BallotRepository {
  // Database methods
  Future<List< BallotEntity>> getBallots();

  Future<void> saveBallot(BallotEntity vote);

  Future<void> removeBallot(BallotEntity vote);
}