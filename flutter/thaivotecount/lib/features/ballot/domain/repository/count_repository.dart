import 'package:thaivotecount/features/ballot/domain/entities/count.dart';

abstract class CountRepository {
  // Database methods
  Future <List<CountEntity>> getCounts(int ballotID);

  Future <void> saveCount(CountEntity vote);
}