import 'package:flutter/material.dart';
import 'package:thaivotecount/features/ballot/presentation/pages/add_ballot/add_ballot.dart';
import 'package:thaivotecount/features/ballot/presentation/pages/ballot_count/ballot_count.dart';
import 'package:thaivotecount/features/ballot/presentation/pages/home/ballot_list.dart';

class AppRoutes {
  static Route onGenerateRoutes(RouteSettings settings) {
    switch (settings.name) {
      case '/':
        return _materialRoute(const BallotList());
      case '/AddBallot':
        return _materialRoute(const AddBallot());
      case '/BallotCount':
      return MaterialPageRoute(
        builder: (BuildContext context) {
          return const BallotCount();
        },
        settings: settings
      );
      default:
        return _materialRoute(const BallotList());
    }
  }

  static Route<dynamic> _materialRoute(Widget view) {
    return MaterialPageRoute(builder: (_) => view);
  }
}