import 'package:flutter/material.dart';

ThemeData theme() {
  return ThemeData(
    scaffoldBackgroundColor: Colors.white,
    fontFamily: 'SukhumvitSet',
    appBarTheme: appBarTheme(),
    colorScheme: ColorScheme.fromSeed(
      seedColor: const Color(0xFF0075e3),
      primary: const Color(0xFF0075e3),
      onPrimary: Colors.black,
      onSecondary: Colors.black38,
      brightness: Brightness.light
    ),
    textTheme: const TextTheme(
      bodyMedium: TextStyle(
        fontFamily: 'SukhumvitSet',
        fontSize: 17.0
      )
    )
  );
}

ThemeData themeDark() {
  return ThemeData(
    scaffoldBackgroundColor: Colors.black,
    fontFamily: 'SukhumvitSet',
    appBarTheme: appBarThemeDark(),
    colorScheme: ColorScheme.fromSeed(
      seedColor: const Color(0xFF0075e3),
      primary: const Color(0xFF0075e3),
      onPrimary: Colors.white,
      onSecondary: Colors.white38,
      brightness: Brightness.dark,
    ),
    textTheme: const TextTheme(
      bodyMedium: TextStyle(
        fontFamily: 'SukhumvitSet',
        fontSize: 17.0
      )
    )
  );
}

AppBarTheme appBarTheme() {
  return const AppBarTheme(
    color: Colors.white,
    elevation: 0,
    centerTitle: true,
    iconTheme: IconThemeData(color: Color(0xFF0075e3)),
    titleTextStyle: TextStyle(
      fontFamily: 'SukhumvitSet',
      color: Colors.black, 
      fontSize: 17.0,
      fontWeight: FontWeight.w600
    )
  );
}

AppBarTheme appBarThemeDark() {
  return const AppBarTheme(
    color: Colors.black,
    elevation: 0,
    centerTitle: true,
    iconTheme: IconThemeData(color: Color(0xFF0075e3)),
    titleTextStyle: TextStyle(
      fontFamily: 'SukhumvitSet',
      color: Colors.white, 
      fontSize: 17.0,
      fontWeight: FontWeight.w600
    )
  );
}