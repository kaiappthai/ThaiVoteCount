import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:thaivotecount/features/ballot/presentation/bloc/ballot/local/local_ballot_bloc.dart';
import 'package:thaivotecount/features/ballot/presentation/pages/home/ballot_list.dart';
import 'config/routes/routes.dart';
import 'config/theme/app_themes.dart';
import 'features/ballot/presentation/bloc/ballot/local/local_ballot_event.dart';
import 'injection_container.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await initializeDependencies();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<LocalBallotBloc>(
      create: (context) => sl()..add(const GetBallots()),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: theme(),
        darkTheme: themeDark(), // standard dark theme
        themeMode: ThemeMode.system,
        onGenerateRoute: AppRoutes.onGenerateRoutes,
        home: const BallotList()
      ),
    );
  }
}
