import 'package:get_it/get_it.dart';
import 'package:dio/dio.dart';
import 'package:thaivotecount/features/ballot/data/data_sources/local/appDatabase/ballot/app_database.dart';
import 'package:thaivotecount/features/ballot/data/repository/ballot_repository_impl.dart';
import 'package:thaivotecount/features/ballot/domain/repository/ballot_repository.dart';
import 'features/ballot/domain/usecases/get_ballots.dart';
import 'features/ballot/domain/usecases/remove_ballot.dart';
import 'features/ballot/domain/usecases/save_ballot.dart';
import 'features/ballot/presentation/bloc/ballot/local/local_ballot_bloc.dart';

final sl = GetIt.instance;

Future<void> initializeDependencies() async {

  final database = await $FloorAppDatabase.databaseBuilder('app_database.db').build();
  sl.registerSingleton<AppDatabase>(database);
  
  // Dio
  sl.registerSingleton<Dio>(Dio());

  // Dependencies
  sl.registerSingleton<BallotRepository>(
    BallotRepositoryImpl(sl())
  );
  
  //UseCases
  sl.registerSingleton<GetBallotsUseCase>(
    GetBallotsUseCase(sl())
  );

  sl.registerSingleton<SaveBallotUseCase>(
    SaveBallotUseCase(sl())
  );
  
  sl.registerSingleton<RemoveBallotUseCase>(
    RemoveBallotUseCase(sl())
  );

  //Blocs
  /*
  sl.registerFactory<RemoteBallotBloc>(
    ()=> RemoteBallotBloc(sl())
  );
*/

  sl.registerFactory<LocalBallotBloc>(
    ()=> LocalBallotBloc(sl(),sl(),sl())
  );

}