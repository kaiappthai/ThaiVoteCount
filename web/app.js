const api = require("./config/api-config");
global.__basedir = __dirname;
const PORT = 80;
const server = api.server.listen(PORT, () => {
    console.log('server connected to port ' + PORT);
});