
function init(app) {
  app.get('/votecount', (req,res) => {
    res.sendFile(__basedir + '/www/votecount/index.html');
  });
  app.get('/votecount/assets/*', (req,res) => {
    res.sendFile(__basedir + '/www' + req.url);
  });
  app.get('/votecount/privacy', (req,res) => {
    res.sendFile(__basedir + '/www/votecount/privacy.html');
  });
  app.get('/votecount/:vid', (req,res) => {
    res.sendFile(__basedir + '/www/votecount/view.html');
  });
  app.get('/votecount/:vid/:filename', (req,res) => {
    res.sendFile(__basedir + '/www/votecount/data/' + req.params.vid + '/' + req.params.filename);
  });
}

module.exports.init = init;