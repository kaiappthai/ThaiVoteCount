const service = require('./service.js');

var multer  = require('multer');
var upload = multer({ dest: 'tmp/' });
var array = upload.array('data');

function init(router) {
    router.route('/votecount/collect')
      .post(array, uploadCount);
}

function uploadCount(req,res) {
    var aData = req.files;
    service.uploadCount(aData).then((data) => {
        res.json(data);
    }).catch((err) => {
        res.json(err);
    });
}

module.exports.init = init;