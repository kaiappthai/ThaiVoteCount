const service = require('./service.js');

function init(router) {
    router.route('/votecount/:vid')
      .get(getData);
}

function getData(req,res) {
    var aData = {};
    aData.vid = req.params.vid;
    service.getData(aData).then((data) => {
        res.json(data);
    }).catch((err) => {
        res.json(err);
    });
}

module.exports.init = init;