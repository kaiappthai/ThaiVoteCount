var fs = require('fs');
var path = require('path');
var uniqid = require('uniqid');
var request = require('request');

var dbFunc = require('../../config/db-function.js');
var db = dbFunc.getdb();

const { resolve } = require('path');

var provinces = ["กรุงเทพมหานคร","กระบี่","กาญจนบุรี","กาฬสินธุ์","กำแพงเพชร","ขอนแก่น","จันทบุรี","ฉะเชิงเทรา","ชลบุรี","ชัยนาท","ชัยภูมิ","ชุมพร","เชียงราย","เชียงใหม่","ตรัง","ตราด","ตาก","นครนายก","นครปฐม","นครพนม","นครราชสีมา","นครศรีธรรมราช","นครสวรรค์","นนทบุรี","นราธิวาส","น่าน","บึงกาฬ","บุรีรัมย์","ปทุมธานี","ประจวบคีรีขันธ์","ปราจีนบุรี","ปัตตานี","พระนครศรีอยุธยา","พังงา","พัทลุง","พิจิตร","พิษณุโลก","เพชรบุรี","เพชรบูรณ์","แพร่","พะเยา","ภูเก็ต","มหาสารคาม","มุกดาหาร","แม่ฮ่องสอน","ยโสธร","ยะลา","ร้อยเอ็ด","ระนอง","ระยอง","ราชบุรี","ลพบุรี","ลำปาง","ลำพูน","เลย","ศรีสะเกษ","สกลนคร","สงขลา","สตูล","สมุทรปราการ","สมุทรสงคราม","สมุทรสาคร","สระแก้ว","สระบุรี","สิงห์บุรี","สุโขทัย","สุพรรณบุรี","สุราษฎร์ธานี","สุรินทร์","หนองคาย","หนองบัวลำภู","อ่างทอง","อุดรธานี","อุตรดิตถ์","อุทัยธานี","อุบลราชธานี","อำนาจเจริญ"];
var types = ["", "การเลือกตั้งทั่วไป 2566 เขต", "การเลือกตั้งทั่วไป 2566 บัญชีรายชื่อ"];

var model = {
  uploadCount: uploadCount,
  getData: getData,
  getFiles: getFiles
};

function uploadCount(aData) {
  console.log("uploadCount");
  console.log(aData);
  return new Promise((resolve,reject) => {
    let ret = {};
    let uqid = uniqid();
    let ct = '';
    aData.forEach(item => {
      if(path.extname(item.originalname) == '.json') {
        ct = fs.readFileSync(item.path, { encoding: 'utf8', flag: 'r' });
      }
    });
    if(ct != '') {
      let json = JSON.parse(ct);
      let item = {};
      item.sid = json.id;
      item.vid = uqid;
      item.name = json.name;
      item.type = json.type;
      item.province = json.province;
      item.region = json.region;
      item.unit = json.unit;
      item.lastnum = json.lastnum;
      item.create_date = json.create_date;
      item.end_date = json.end_date;
      let sql = 'INSERT into count set ?';
      db.query(sql,[item],(err, result)=>{
        if(err){
          console.log(err);
          ret.result = false;
          ret.error = err;
          resolve(ret);
        }else{
          let dir = 'www/votecount/data/' + uqid;
          if (!fs.existsSync(dir)){
            fs.mkdirSync(dir);
          }
          aData.forEach(item => {
            var oldPath = item.path;
            var newPath = dir + '/' + item.originalname;
            fs.renameSync(oldPath, newPath);
          });
          ret.result = true;
          ret.url = 'http://localhost/votecount/' + uqid;
          resolve(ret);
        }
      });
    } else {
      ret.result = false;
      ret.error = "no json file";
      resolve(ret);
    }
  });
}

function getData(aData) {
  return new Promise((resolve,reject) => {
    let ret = {};
    getFiles(aData).then((data) => {
      ret.files = data.files;
      data.files.forEach(file => {
        if(path.extname(file) == '.json') {
          let ct = fs.readFileSync('www/votecount/data/' + aData.vid + '/' + file, { encoding: 'utf8', flag: 'r' });
          let json = JSON.parse(ct);
          let item = {};
          item.sid = json.id;
          item.vid = aData.vid;
          item.name = json.name;
          item.type = parseInt(json.type);
          item.province = json.province;
          item.region = json.region;
          item.unit = json.unit;
          item.lastnum = parseInt(json.lastnum);
          item.create_date = json.create_date;
          item.end_date = json.end_date;
          ret.profile = item;
        } else if(path.extname(file) == '.csv') {
          let ct = fs.readFileSync('www/votecount/data/' + aData.vid + '/' + file, { encoding: 'utf8', flag: 'r' }).split('\n');
          ret.vote_record = [];
          ct.forEach(ctx => {
            let ctxx = ctx.split(',');
            let cti = {};
            cti.id = parseInt(ctxx[0]);
            cti.num = parseInt(ctxx[1]);
            cti.count = parseInt(ctxx[2]);
            cti.date = ctxx[3];
            ret.vote_record.push(cti);
          });
        }
      });
      let vres = [];
      for(let i = 0; i <= ret.profile.lastnum; i++) {
        vres[i] = 0;
      }
      ret.vote_record.forEach(vr => {
        vres[vr.num] += vr.count;
      });
      ret.vote_result = vres;
      ret.result = true;
      resolve(ret);
    });
  });
}

function getFiles(aData) {
  return new Promise((resolve,reject) => {
    let ret = {};
    ret.result = true;
    ret.files = fs.readdirSync('www/votecount/data/' + aData.vid);
    resolve(ret);
  });
}

module.exports = model;
