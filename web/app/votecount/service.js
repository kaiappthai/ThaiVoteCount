var model = require("./model.js");

var service = {
    uploadCount: uploadCount,
    getData: getData,
    getFiles: getFiles
};

function uploadCount(aData) {
    return new Promise((resolve,reject) => {
        model.uploadCount(aData).then((data)=>{
          console.log('Upload success.');
            resolve(data);
        }).catch((err) => {
            console.log('Upload fail.');
            reject(err);
        })
    });
}

function getData(aData) {
    return new Promise((resolve,reject) => {
        model.getData(aData).then((data)=>{
            resolve(data);
        }).catch((err) => {
            reject(err);
        })
    });
}

function getFiles(aData) {
    return new Promise((resolve,reject) => {
        model.getFiles(aData).then((data)=>{
            resolve(data);
        }).catch((err) => {
            reject(err);
        })
    });
}

module.exports = service;