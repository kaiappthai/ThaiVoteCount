var express = require("express");
var app = express();
var http = require('http');
var fs = require('fs');
var server = http.createServer(app);
var path  = require('path');
var bodyParser = require('body-parser');
var cors = require('cors');
var checkToken = require('./secureRoute');
app.use(cors());

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Credentials", "true");
    res.header("Access-Control-Allow-Methods", 'PUT, GET, POST, DELETE, OPTIONS');
    res.header(
      "Access-Control-Allow-Headers","Origin, X-Requested-With, Content-Type, Accept, Authorization, secret"
    );
    next();
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
var router = express.Router();
app.use('/api',router);

var secureApi = express.Router();

//set static folder
app.use(express.static(path.join(__dirname, 'assets')));

//body parser middleware
app.use('/api',secureApi);
secureApi.use(checkToken);

app.use(function (err, req, res, next) {
  console.error(err.stack);
  res.status(500).send('Something went wrong! Please try after sometime.');
});

// index route
app.get('/', (req,res) => {
  res.sendFile(__basedir + '/www/index.html');
});

app.get('/assets/*', (req,res) => {
    res.sendFile(__basedir + req.url);
});

var ApiConfig = {
  app: app,
  server: server
};

var voteCon = require('../app/votecount/controller.js');
voteCon.init(secureApi);

var voteConPublic = require('../app/votecount/public.js');
voteConPublic.init(router);

var webCon = require('../www/votecount.js');
webCon.init(app);

module.exports = ApiConfig;
