const mysql = require('mysql');
const dbs = require('./dbs.json');
const default_db = 'thaivote';

let db = null;

function setdb(dbname) {
  if (typeof dbs != 'undefined') {
    if (typeof(dbname) == 'undefined') {
      dbname = default_db;
    }
    //console.log(dbname);
    if (dbs.hasOwnProperty(dbname)) {
      let db_config = dbs[dbname];
      let dbopt = {
        connectionLimit: db_config.connectionLimit,
        host: db_config.host,
        user: db_config.user,
        password: db_config.password,
        database: db_config.database,
        port: db_config.port,
        sslmode: 'REQUIRED'
      };
      if (Object.keys(db_config.ssl).length != 0) {
        dbopt.ssl = db_config.ssl;
      }
      db = mysql.createPool(dbopt);
    }
  }
}

function getdb() {
  if (db == null) {
    setdb(default_db);
  }
  return db;
}

function connectionCheck() {
  return new Promise((resolve, reject) => {
    if (db == null) {
      setdb(default_db);
    }
    db.getConnection(function(err, connection) {
      if (err) {
        if (connection) connection.release();
        reject(err);
      } else {
        resolve('Connection successful!');
      }
    });
  });
}

function connectionRelease() {
  if (db == null) {
    setdb(default_db);
  }
  if (db != null) {
    db.on('release', function(connection) {
      // console.log('Connection %d released', connection.threadId);
    });
  }
}

module.exports = {
  getdb: function(dbname) {
    return getdb(dbname);
  },
  setdb: function(dbname) {
    return setdb(dbname);
  },
  connectionCheck: connectionCheck(),
  connectionRelease: connectionRelease()
};
