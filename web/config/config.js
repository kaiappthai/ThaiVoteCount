var os = require("os");
var hostname = os.hostname();

module.exports= {
    APP_SECRET:'APP_SECRET',
    BASE_URL:`https://${hostname}/`
};
