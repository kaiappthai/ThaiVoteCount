const jwt = require('jsonwebtoken');
const config = require('./config.js');
var crypto = require('crypto');

module.exports = function checkToken(req, res, next) {
    var token = req.headers['token'];
    if (token && token.startsWith("Bearer ")){
      token = token.substring(7, token.length);
    }
    var auth = req.headers['authorization'] ? req.headers['authorization'] :'';
    if(auth){
      var vhash = crypto.createHash('sha256').update(config.APP_SECRET).digest('hex');
      if(auth == vhash) {
        next();
      } else {
        res.json({"status":500,
          "message":"INVALID TOKEN",
          "error":"Invalid authorization"
       });
      }
    }else if(token) {
      jwt.verify(token, config.APP_SECRET, (err, decode)=>{
        if(err) {
          res.json({"status":500,
            "message":"INVALID TOKEN",
            "error":err.message
          });
        } else { 
          decode.token = token;
          req.payload = decode;
          next();
        }
      });
    } else {
      res.json({
        "status": 500,
        "message": "NO TOKEN PROVIDED",
        "error": "token must be provide in header for endpoint access"
      });
    }
};


