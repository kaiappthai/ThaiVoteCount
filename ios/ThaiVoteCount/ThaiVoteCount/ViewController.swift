//
//  ViewController.swift
//  ThaiVoteCount
//
//  Created by Thosapol Sangkasub on 6/5/23.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var notItemLB: UILabel!
    @IBOutlet weak var notItemButton: UIButton!
    
    let sqlHelper = SQLiteHelper()
    var votes: [VoteModel] = []
    var db: OpaquePointer? = nil
    var sendItem: VoteModel = VoteModel()
    let recEngine = RecordEngine()

    override func viewDidLoad() {
        super.viewDidLoad()
        if sqlHelper.checkMainDB() {
            db = sqlHelper.getMainDB()!
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateTable()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goVoteRecord" {
            if let vc = segue.destination as? RecordCount {
                vc.voteItem = sendItem
                vc.recEngine = recEngine
            }
        }
    }
    
    func updateTable() {
        if let db2: OpaquePointer = db {
            votes = sqlHelper.getMainData(db: db2)
            if votes.count > 0 {
                notItemLB.alpha = 0
                notItemButton.alpha = 0
            } else {
                notItemLB.alpha = 1
                notItemButton.alpha = 1
            }
            tableView.reloadData()
        }
    }
    
    @IBAction func tapAdd() {
        performSegue(withIdentifier: "goCreateRecord", sender: nil)
    }
}

extension ViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return votes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "votecell", for: indexPath)
        let vm: VoteModel = votes[indexPath.row]
        let tt: UILabel = cell.viewWithTag(100) as! UILabel
        tt.text = "\(getTypeTitle(id: vm.type))\n\(vm.province) เขต \(vm.region) หน่วย \(vm.unit)"
        return cell
    }
}

extension ViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        sendItem = votes[indexPath.row]
        performSegue(withIdentifier: "goVoteRecord", sender: nil)
    }
}

