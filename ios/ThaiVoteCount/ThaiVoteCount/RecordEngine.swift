//
//  RecordEngine.swift
//  ThaiVoteCount
//
//  Created by Thosapol Sangkasub on 7/5/23.
//

import Foundation
import AVFoundation

class RecordEngine {
    var audioDir: URL?
    let engine = AVAudioEngine()
    var isTap: Bool = false

    var chunkFile: AVAudioFile! = nil
    var chunkFrames: AVAudioFrameCount = 0
    
    func prepare(vid: Int) {
        audioDir = getApplicationSupportDirectory()!.appendingPathComponent("\(vid)", isDirectory: true)
    }
    
    func doRec(vid: Int) {
        configureAudioSession()
        if !isTap {
            let input = engine.inputNode
            let bus = 0
            let inputFormat = input.inputFormat(forBus: bus)
            input.installTap(onBus: bus, bufferSize: 512, format: inputFormat) { (buffer, time) -> Void in
                DispatchQueue.main.async {
                    self.writeBuffer(buffer)
                }
            }
            isTap = true
        }
        try! engine.start()
    }
    
    func stopRec() {
        if engine.isRunning {
            engine.stop()
            chunkFile = nil
        }
    }
    
    func configureAudioSession() {
        let session = AVAudioSession.sharedInstance()
        do {
            try session.setCategory(.record, mode: .default)
            try session.setActive(true)
            try session.setMode(.voiceChat)
            try session.setPreferredSampleRate(22050.0)
            try session.setPreferredIOBufferDuration(0.005)
        } catch {
            print(error)
        }
    }
    
    func writeBuffer(_ buffer: AVAudioPCMBuffer) {
        let samplesPerSecond = buffer.format.sampleRate
        if chunkFile == nil {
            createNewChunkFile(numChannels: buffer.format.channelCount, samplesPerSecond: samplesPerSecond)
        }
        try! chunkFile.write(from: buffer)
        chunkFrames += buffer.frameLength
    }

    func createNewChunkFile(numChannels: AVAudioChannelCount, samplesPerSecond: Float64) {
        let fileUrl = audioDir!.appendingPathComponent("audio_\(dateToday().replacingOccurrences(of: " ", with: "_").replacingOccurrences(of: ":", with: "-"))_\(Date().timeIntervalSince1970).aac")
        print("writing chunk to \(fileUrl)")
        print("numChannels = \(numChannels)")
        print("samplesPerSecond = \(samplesPerSecond)")
        let settings: [String: Any] = [
            AVFormatIDKey: kAudioFormatMPEG4AAC,
            AVEncoderAudioQualityKey: AVAudioQuality.min,
            AVEncoderBitRateKey: 32000,
            AVNumberOfChannelsKey: numChannels,
            AVSampleRateKey: samplesPerSecond
        ]
        chunkFile = try! AVAudioFile(forWriting: fileUrl, settings: settings)
        chunkFrames = 0
    }
}
