//
//  VoteModel.swift
//  ThaiVoteCount
//
//  Created by Thosapol Sangkasub on 7/5/23.
//

import Foundation

class VoteModel {
    var id: Int = 0
    var name: String = ""
    var type: Int = 0
    var province: String = ""
    var region: Int = 0
    var unit: Int = 0
    var lastnum: Int = 0
    var is_end: Int = 0
    var url: String = ""
    var create_date: String = ""
    var end_date: String = ""
    
    init() {}
    
    func toJsonString() -> String {
        var dict = Dictionary<String, Any>()
        dict["id"] = id
        dict["name"] = name
        dict["type"] = type
        dict["province"] = province
        dict["region"] = region
        dict["unit"] = unit
        dict["lastnum"] = lastnum
        dict["create_date"] = create_date
        dict["end_date"] = end_date
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: dict, options: .fragmentsAllowed)
            let jsonstr = String(data: jsonData, encoding: .utf8)
            if let out: String = jsonstr {
                return out
            } else {
                return "{}"
            }
        } catch {
            print(error.localizedDescription)
            return "{}"
        }
    }
}
