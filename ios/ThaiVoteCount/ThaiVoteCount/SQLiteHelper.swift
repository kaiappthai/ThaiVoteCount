//
//  SQLiteHelper.swift
//  ThaiVoteCount
//
//  Created by Thosapol Sangkasub on 7/5/23.
//

import Foundation
import SQLite3

class SQLiteHelper {
    
    let SQLITE_TRANSIENT = unsafeBitCast(-1, to: sqlite3_destructor_type.self)
    
    func checkMainDB() -> Bool {
        let appSupportFolder: URL = getApplicationSupportDirectory()!
        let dbPath = appSupportFolder.appendingPathComponent("thaivote.sqlite", isDirectory: false)
        var db:OpaquePointer? = nil
        if sqlite3_open(dbPath.path(), &db) == SQLITE_OK {
            var statement: OpaquePointer?
            let createTableString = "create table if not exists mdata ( id INT PRIMARY KEY NOT NULL, name TEXT, type INT, province TEXT, region INT, unit INT, lastnum INT, is_end INT, url TEXT, create_date TEXT, end_date TEXT)"
            if sqlite3_prepare(db, createTableString, -1, &statement, nil) == SQLITE_OK {
                if sqlite3_step(statement) == SQLITE_DONE {
                    sqlite3_finalize(statement)
                    sqlite3_close(db)
                    return true
                } else {
                    print("table is not created.")
                    sqlite3_finalize(statement)
                    sqlite3_close(db)
                    return false
                }
            } else {
                print("invalid sql: \(createTableString)")
                sqlite3_finalize(statement)
                sqlite3_close(db)
                return false
            }
        } else {
            print("db thaivote is not created.")
            return false
        }
    }
    
    func getMainDB() -> OpaquePointer?{
        let appSupportFolder: URL = getApplicationSupportDirectory()!
        let dbPath = appSupportFolder.appendingPathComponent("thaivote.sqlite", isDirectory: false)
        var db:OpaquePointer? = nil
        if sqlite3_open(dbPath.path(), &db) == SQLITE_OK {
            return db
        } else {
            return nil
        }
    }
    
    enum SQLiteError: Error {
        case open(result: Int32)
        case exec(message: String)
        case prepare(message: String)
        case bind(message: String)
        case step(message: String)
        case column(message: String)
        case invalidDate
        case missingData
        case noDataChanged
    }
    
    func saveMainData(db: OpaquePointer, newData: VoteModel, completion: @escaping ((Bool) -> Void)) {
        var statement: OpaquePointer?
        let id: Int = newData.id
        let name: String = newData.name
        let type: Int = newData.type
        let province: String = newData.province
        let region: Int = newData.region
        let unit: Int = newData.unit
        let lastnum: Int = newData.lastnum
        let is_end: Int = newData.is_end
        let url: String = newData.url
        let create_date: String = newData.create_date
        let end_date: String = newData.end_date
        print("id = \(id), name = \(name), type = \(type), province = \(province), unit = \(unit), region = \(region), lastnum = \(lastnum), is_end = \(is_end), url = \(url), create_date = \(create_date), end_date = \(end_date)")
        let insertStatementString = "INSERT INTO mdata (id, name, type, province, region, unit, lastnum, is_end, url, create_date, end_date) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
        if sqlite3_prepare(db, insertStatementString, -1, &statement, nil) == SQLITE_OK {
            sqlite3_bind_int64(statement, 1, Int64(id))
            sqlite3_bind_text(statement, 2, name, -1, SQLITE_TRANSIENT)
            sqlite3_bind_int64(statement, 3, Int64(type))
            sqlite3_bind_text(statement, 4, province, -1, SQLITE_TRANSIENT)
            sqlite3_bind_int64(statement, 5, Int64(region))
            sqlite3_bind_int64(statement, 6, Int64(unit))
            sqlite3_bind_int64(statement, 7, Int64(lastnum))
            sqlite3_bind_int64(statement, 8, Int64(is_end))
            sqlite3_bind_text(statement, 9, url, -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 10, create_date, -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 11, end_date, -1, SQLITE_TRANSIENT)
            if sqlite3_step(statement) == SQLITE_DONE {
                sqlite3_finalize(statement)
                completion(true)
            } else {
                print("invalid2 sql: \(insertStatementString)")
                sqlite3_finalize(statement)
                completion(false)
            }
        } else {
            print("invalid sql: \(insertStatementString)")
            completion(false)
        }
    }
    
    func updateMainData(db: OpaquePointer, id: Int, field: String, value: Any) -> Bool {
        var statement: OpaquePointer?
        var updateStatementString = ""
        if field == "is_end" {
            updateStatementString = "UPDATE mdata SET \(field) = \(value as! Int) WHERE id = \(id)"
        } else if field == "end_date" {
            updateStatementString = "UPDATE mdata SET \(field) = '\(value as! String)' WHERE id = \(id)"
        } else if field == "url" {
            updateStatementString = "UPDATE mdata SET \(field) = '\(value as! String)' WHERE id = \(id)"
        }
        if sqlite3_prepare(db, updateStatementString, -1, &statement, nil) == SQLITE_OK {
            if sqlite3_step(statement) == SQLITE_DONE {
                sqlite3_finalize(statement)
                return true
            } else {
                sqlite3_finalize(statement)
                return false
            }
        } else {
            print("invalid sql: \(updateStatementString)")
            return false
        }
    }
    
    func deleteMainData(db: OpaquePointer, id: Int) -> Bool {
        var statement: OpaquePointer?
        let deleteStatement = "DELETE FROM mdata WHERE id = \(id)"
        if sqlite3_prepare(db, deleteStatement, -1, &statement, nil) == SQLITE_OK {
            if sqlite3_step(statement) == SQLITE_DONE {
                sqlite3_finalize(statement)
                return true
            } else {
                sqlite3_finalize(statement)
                return false
            }
        } else {
            print("invalid sql: \(deleteStatement)")
            return false
        }
    }
    
    func getMainData(db: OpaquePointer) -> [VoteModel] {
        var statement: OpaquePointer?
        var items = [VoteModel]()
        let selectQry = "SELECT * FROM mdata ORDER BY id DESC "
        if sqlite3_prepare(db, selectQry, -1, &statement, nil) == SQLITE_OK {
            while sqlite3_step(statement) == SQLITE_ROW {
                let item: VoteModel = .init()
                item.id = Int(sqlite3_column_int64(statement, 0))
                item.name = String(cString: sqlite3_column_text(statement, 1))
                item.type = Int(sqlite3_column_int64(statement, 2))
                item.province = String(cString: sqlite3_column_text(statement, 3))
                item.region = Int(sqlite3_column_int64(statement, 4))
                item.unit = Int(sqlite3_column_int64(statement, 5))
                item.lastnum = Int(sqlite3_column_int64(statement, 6))
                item.is_end = Int(sqlite3_column_int64(statement, 7))
                item.url = String(cString: sqlite3_column_text(statement, 8))
                item.create_date = String(cString: sqlite3_column_text(statement, 9))
                item.end_date = String(cString: sqlite3_column_text(statement, 10))
                items.append(item)
            }
            sqlite3_finalize(statement)
        }
        return items
    }
    
    func checkDB(recid: Int) -> Bool {
        let appSupportFolder: URL = getApplicationSupportDirectory()!
        let dbFolderPath = appSupportFolder.appendingPathComponent("\(recid)", isDirectory: true)
        if !FileManager.default.fileExists(atPath: dbFolderPath.path) {
            do {
                try FileManager.default.createDirectory(at: dbFolderPath, withIntermediateDirectories: true, attributes: nil)
            } catch let error {
                print(error)
            }
        }
        let dbPath = dbFolderPath.appendingPathComponent("\(recid).sqlite", isDirectory: false)
        var db:OpaquePointer? = nil
        if sqlite3_open(dbPath.path(), &db) == SQLITE_OK {
            var statement: OpaquePointer?
            let createTableString = "create table if not exists vdata( id INT PRIMARY KEY NOT NULL, vnum INT, vadd INT, vdate TEXT)"
            if sqlite3_prepare(db, createTableString, -1, &statement, nil) == SQLITE_OK {
                if sqlite3_step(statement) == SQLITE_DONE {
                    sqlite3_finalize(statement)
                    sqlite3_close(db)
                    return true
                } else {
                    print("table is not created.")
                    sqlite3_finalize(statement)
                    sqlite3_close(db)
                    return false
                }
            } else {
                print("invalid sql: \(createTableString)")
                sqlite3_finalize(statement)
                sqlite3_close(db)
                return false
            }
        } else {
            print("db \(recid) is not created.")
            return false
        }
    }
    
    func getDB(recid: Int) -> OpaquePointer?{
        let appSupportFolder: URL = getApplicationSupportDirectory()!
        let dbFolderPath = appSupportFolder.appendingPathComponent("\(recid)", isDirectory: true)
        let dbPath = dbFolderPath.appendingPathComponent("\(recid).sqlite", isDirectory: false)
        var db:OpaquePointer? = nil
        if sqlite3_open(dbPath.path(), &db) == SQLITE_OK {
            return db
        } else {
            return nil
        }
    }
    
    func closeDB(db: OpaquePointer) {
        sqlite3_close(db)
    }
    
    func saveRecData(db: OpaquePointer, newData: CountModel) -> Bool {
        var statement: OpaquePointer?
        let id: Int = newData.id
        let vnum: Int = newData.vnum
        let vadd: Int = newData.vadd
        let vdate: String = newData.vdate
        let insertStatementString = "INSERT INTO vdata (id, vnum, vadd, vdate) VALUES (?, ?, ?, ?)"
        if sqlite3_prepare(db, insertStatementString, -1, &statement, nil) == SQLITE_OK {
            sqlite3_bind_int64(statement, 1, Int64(id))
            sqlite3_bind_int64(statement, 2, Int64(vnum))
            sqlite3_bind_int64(statement, 3, Int64(vadd))
            sqlite3_bind_text(statement, 4, vdate, -1, SQLITE_TRANSIENT)
            if sqlite3_step(statement) == SQLITE_DONE {
                sqlite3_finalize(statement)
                return true
            } else {
                print("invalid sql: \(insertStatementString)")
                sqlite3_finalize(statement)
                return false
            }
        } else {
            print("invalid sql: \(insertStatementString)")
            return false
        }
    }
    
    func getRecDB(db: OpaquePointer) -> Array<CountModel>{
        var statement: OpaquePointer?
        var items: Array = Array<CountModel>()
        let selectQry = "SELECT * FROM vdata ORDER BY id ASC "
        if sqlite3_prepare(db, selectQry, -1, &statement, nil) == SQLITE_OK {
            while sqlite3_step(statement) == SQLITE_ROW {
                let item: CountModel = .init()
                item.id = Int(sqlite3_column_int64(statement, 0))
                item.vnum = Int(sqlite3_column_int64(statement, 1))
                item.vadd = Int(sqlite3_column_int64(statement, 2))
                item.vdate = String(cString: sqlite3_column_text(statement, 3))
                items.append(item)
            }
            sqlite3_finalize(statement)
        }
        return items
    }
}
