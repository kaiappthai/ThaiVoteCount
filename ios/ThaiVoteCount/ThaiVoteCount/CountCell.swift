//
//  CountCell.swift
//  ThaiVoteCount
//
//  Created by Thosapol Sangkasub on 7/5/23.
//

import UIKit

class CountCell: UITableViewCell {
    @IBOutlet weak var cNum: UILabel!
    @IBOutlet weak var cCount: UILabel!
    @IBOutlet weak var cPlus: UIButton!
    @IBOutlet weak var cMinus: UIButton!
    @IBOutlet weak var cView: UIView!
}
