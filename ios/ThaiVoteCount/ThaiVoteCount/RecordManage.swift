//
//  RecordManage.swift
//  ThaiVoteCount
//
//  Created by Thosapol Sangkasub on 8/5/23.
//

import UIKit

class RecordManage: UIViewController {
    
    @IBOutlet weak var sTitleLB: UILabel!
    @IBOutlet weak var confirmView: UIView!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var countView: UIView!
    @IBOutlet weak var countLB: UILabel!
    @IBOutlet weak var audioView: UIView!
    @IBOutlet weak var audioLB: UILabel!
    @IBOutlet weak var sendServerView: UIView!
    @IBOutlet weak var onlineView: UIView!
    @IBOutlet weak var onlineLB: UILabel!
    @IBOutlet weak var countButh: NSLayoutConstraint!
    @IBOutlet weak var audioButh: NSLayoutConstraint!
    @IBOutlet weak var sendServerBut: UIButton!
    @IBOutlet weak var deleteBut: UIButton!
    
    let sqlHelper = SQLiteHelper()
    var voteItem: VoteModel = VoteModel()
    var recEngine = RecordEngine()
    var allcount: [CountModel] = []
    var allaudio: [URL] = []
    var allupload: [URL] = []
    var tdir: URL?
    
    let uploadURL = "http://localhost/api/votecount/collect"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tdir = getApplicationSupportDirectory()!.appendingPathComponent("\(voteItem.id)", isDirectory: true)
        updateView()
        getAllCount()
        getAllAudio()
    }
    
    func updateView() {
        sTitleLB.text = "\(getTypeTitle(id: voteItem.type))\n\(voteItem.province) เขต \(voteItem.region) หน่วย \(voteItem.unit)"
        if voteItem.is_end == 1 {
            confirmView.isHidden = true
            statusView.isHidden = false
            countButh.constant = 114.0
            if allaudio.count > 0 {
                audioButh.constant = 114.0
            } else {
                audioButh.constant = 0.0
            }
            if !voteItem.url.isEmpty {
                onlineLB.text = "ดูการนับคะแนนของคุณได้ที่\n\(voteItem.url)"
                sendServerView.isHidden = true
                onlineView.isHidden = false
            } else {
                sendServerView.isHidden = false
                onlineView.isHidden = true
            }
        } else {
            confirmView.isHidden = false
            statusView.isHidden = true
            countButh.constant = 0.0
            audioButh.constant = 0.0
            sendServerView.isHidden = true
            sendServerView.isHidden = true
            onlineView.isHidden = true
        }
        deleteBut.tintColor = UIColor.red
    }
    
    func getAllCount() {
        if sqlHelper.checkDB(recid: voteItem.id) {
            let db = sqlHelper.getDB(recid: voteItem.id)
            allcount = sqlHelper.getRecDB(db: db!)
            var acno = 0
            for c in allcount {
                acno += c.vadd
            }
            countLB.text = "\(acno)"
        }
    }
    
    func getAllAudio() {
        do {
            let directoryContents: [URL] = try FileManager.default.contentsOfDirectory( at: tdir!, includingPropertiesForKeys: nil)
            for dc in directoryContents {
                if dc.lastPathComponent.starts(with: "audio_") {
                    allaudio.append(dc)
                }
                if dc.pathExtension == "json" || dc.pathExtension == "csv" || dc.pathExtension == "aac" {
                    allupload.append(dc)
                }
            }
        } catch {
            print(error)
        }
        audioLB.text = "\(allaudio.count) ไฟล์"
    }
    
    func saveJsonFile() {
        let vjson = voteItem.toJsonString()
        let fnpath = tdir!.appendingPathComponent("\(voteItem.id).json" , isDirectory: false)
        do {
            try vjson.write(to: fnpath, atomically: false, encoding: .utf8)
        } catch let error {
            print(error)
        }
    }
    
    func saveCSVFile(){
        var csvtxt = "id,candidate,count,date"
        for ct in allcount {
            csvtxt += "\n\(ct.id),\(ct.vnum),\(ct.vadd),\(ct.vdate)"
        }
        let fnpath = tdir!.appendingPathComponent("\(voteItem.id).csv" , isDirectory: false)
        do {
            try csvtxt.write(to: fnpath, atomically: false, encoding: .utf8)
        } catch let error {
            print(error)
        }
    }
    
    @IBAction func tapEnd() {
        voteItem.is_end = 1
        voteItem.end_date = dateToday()
        saveJsonFile()
        saveCSVFile()
        if sqlHelper.checkMainDB() {
            let db2 = sqlHelper.getMainDB()!
            if sqlHelper.updateMainData(db: db2, id: voteItem.id, field: "is_end", value: voteItem.is_end) {
                if sqlHelper.updateMainData(db: db2, id: voteItem.id, field: "end_date", value: voteItem.end_date) {
                    updateView()
                }
            }
        }
    }
    
    @IBAction func tapOnline() {
        if !voteItem.url.isEmpty {
            if let url = URL(string: voteItem.url), UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url)
            }
        }
    }
    
    @IBAction func shareCSV() {
        let fileURL = tdir!.appendingPathComponent("\(voteItem.id).csv" , isDirectory: false)
        var filesToShare = [Any]()
        filesToShare.append(fileURL)
        let activityViewController = UIActivityViewController(activityItems: filesToShare, applicationActivities: nil)
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    @IBAction func shareAudio() {
        var filesToShare = [Any]()
        for audio in allaudio {
            filesToShare.append(audio)
        }
        let activityViewController = UIActivityViewController(activityItems: filesToShare, applicationActivities: nil)
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    @IBAction func deleteData() {
        if sqlHelper.checkMainDB() {
            let db2 = sqlHelper.getMainDB()!
            if sqlHelper.deleteMainData(db: db2, id: voteItem.id) {
                do {
                    try FileManager.default.removeItem(at: tdir!)
                } catch(let error) {
                    print(error)
                }
                self.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
    
    @IBAction func shareServer() {
        uploadFiles(allupload) { response in
            print(response ?? "nil")
            if let res: [String: Any] = response, let result: Bool = res["result"] as? Bool, result, let vurl: String = res["url"] as? String {
                print(vurl)
                self.voteItem.url = vurl
                if self.sqlHelper.checkMainDB() {
                    let db2 = self.sqlHelper.getMainDB()!
                    if self.sqlHelper.updateMainData(db: db2, id: self.voteItem.id, field: "url", value: self.voteItem.url) {
                        DispatchQueue.main.async {
                            self.updateView()
                        }
                    }
                }
            }
        }
    }
    
    func uploadFiles(_ urlPath: [URL], completion: @escaping (([String:Any]?) -> Void)){
        if let url = URL(string: uploadURL){
            var request = URLRequest(url: url)
            let boundary:String = "Boundary-\(UUID().uuidString)"
            request.httpMethod = "POST"
            request.timeoutInterval = 600
            request.setValue("9e8b8ada7aee61499181a06716423a814104931b79fcd2d867f0e61e23c11078", forHTTPHeaderField: "Authorization")
            request.allHTTPHeaderFields = ["Content-Type": "multipart/form-data; boundary=----\(boundary)"]
            var data: Data = Data()
            for path in urlPath{
                do {
                    var data2: Data = Data()
                    data2 = try .init(contentsOf: path)
                    data.append("------\(boundary)\r\n")
                    data.append("Content-Disposition: form-data; name=\"data\"; filename=\"\(path.lastPathComponent)\"\r\n")
                    data.append("Content-Type: application/octet-stream\r\n\r\n")
                    data.append(data2)
                    data.append("\r\n")
                } catch let error {
                    print("error here")
                    print(error)
                }
            }
            data.append("------\(boundary)--")
            request.httpBody = data
            DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).sync {
                URLSession.shared.dataTask(with: request, completionHandler: { (dataS, aResponse, error) in
                    if let erros = error {
                        print(erros)
                        completion(nil)
                    } else {
                        do {
                            let responseObj = try JSONSerialization.jsonObject(with: dataS!, options: JSONSerialization.ReadingOptions(rawValue:0)) as! [String:Any]
                            completion(responseObj)
                        } catch let error {
                            print(error)
                            completion(nil)
                        }
                    }
                }).resume()
            }
        } else {
            completion(nil)
        }
    }
}

extension Data{
    mutating func append(_ string: String, using encoding: String.Encoding = .utf8) {
        if let data = string.data(using: encoding) {
            append(data)
        }
    }
}
