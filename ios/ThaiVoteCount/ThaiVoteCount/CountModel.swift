//
//  CountModel.swift
//  ThaiVoteCount
//
//  Created by Thosapol Sangkasub on 7/5/23.
//

import Foundation

class CountModel {
    var id: Int = 0
    var vnum: Int = 0
    var vadd: Int = 0
    var vdate: String = ""
    
    init() {}
    
    init(vnum: Int, vadd: Int) {
        self.vnum = vnum
        self.vadd = vadd
        self.vdate = dateToday()
    }
    
    private func dateToday() -> String {
        let date = Date()
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone(identifier: "Asia/Bangkok")!
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateString = formatter.string(from: date)
        return dateString
    }
}
