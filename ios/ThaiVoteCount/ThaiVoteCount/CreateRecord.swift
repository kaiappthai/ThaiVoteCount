//
//  CreateRecord.swift
//  ThaiVoteCount
//
//  Created by Thosapol Sangkasub on 7/5/23.
//

import UIKit

class CreateRecord: UIViewController {
    
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var typeButton: UIButton!
    @IBOutlet weak var provinceButton: UIButton!
    @IBOutlet weak var regionTF: UITextField!
    @IBOutlet weak var regionLB: UILabel!
    @IBOutlet weak var unitTF: UITextField!
    @IBOutlet weak var unitLB: UILabel!
    @IBOutlet weak var lastNumButton: UIButton!
    
    let sqlHelper = SQLiteHelper()
    let voteItem = VoteModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTypeButton()
        setupProvinceButton()
        setupLastNumButton()
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func setupTypeButton() {
        let closure = { (action: UIAction) in
            if let typeVal: Int = Int(action.identifier.rawValue) {
                self.voteItem.type = typeVal
            }
        }
        var typearr: [UIAction] = []
        for tid in typeIDs {
            typearr.append(UIAction(title: getTypeTitle(id: tid), identifier: .init("\(tid)"), handler: closure))
        }
        typeButton.menu = UIMenu(children: typearr)
        typeButton.showsMenuAsPrimaryAction = true
        self.voteItem.type = typeIDs[0]
    }
    
    func setupProvinceButton() {
        let closure = { (action: UIAction) in
            self.voteItem.province = action.title
        }
        var typearr: [UIAction] = []
        for pv in provinces {
            typearr.append(UIAction(title: pv, handler: closure))
        }
        provinceButton.menu = UIMenu(children: typearr)
        provinceButton.showsMenuAsPrimaryAction = true
        self.voteItem.province = provinces[0]
    }
    
    func setupLastNumButton() {
        let closure = { (action: UIAction) in
            self.voteItem.lastnum = Int(action.title) ?? 0
        }
        var typearr: [UIAction] = []
        for num in 2...100 {
            typearr.append(UIAction(title: "\(num)", handler: closure))
        }
        lastNumButton.menu = UIMenu(children: typearr)
        lastNumButton.showsMenuAsPrimaryAction = true
        self.voteItem.lastnum = 2
    }
    
    @IBAction func tapSubmit() {
        voteItem.name = nameTF.text ?? ""
        if let region: String = regionTF.text, !region.isEmpty, let reg: Int = Int(region){
            voteItem.region = reg
            regionLB.alpha = 0
        } else {
            regionLB.alpha = 1
        }
        if let unit: String = unitTF.text, !unit.isEmpty, let uni: Int = Int(unit){
            voteItem.unit = uni
            unitLB.alpha = 0
        } else {
            unitLB.alpha = 1
        }
        voteItem.id = Int(Date().timeIntervalSince1970)
        voteItem.create_date = dateToday()
        saveVote()
    }
    
    func saveVote() {
        if sqlHelper.checkMainDB() {
            let db = sqlHelper.getMainDB()!
            sqlHelper.saveMainData(db: db, newData: voteItem) { result in
                print("result = \(result)")
                if result {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
}

extension CreateRecord: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.tag == 77 || textField.tag == 78 {
            let allowedCharacters = CharacterSet.decimalDigits
            let characterSet = CharacterSet(charactersIn: string)
            return allowedCharacters.isSuperset(of: characterSet)
        } else {
            return true
        }
    }
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if sender != nil, let asender: UITextField = sender as? UITextField, (asender.tag == 77 || asender.tag == 78), action == #selector(UIResponderStandardEditActions.paste(_:)) {
            return false
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
       textField.resignFirstResponder()
       return true
    }
}
