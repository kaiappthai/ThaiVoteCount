//
//  RecordCount.swift
//  ThaiVoteCount
//
//  Created by Thosapol Sangkasub on 7/5/23.
//

import UIKit

class RecordCount: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var sTitleLB: UILabel!
    @IBOutlet weak var recBut: UIButton!
    @IBOutlet weak var manageBut: UIButton!
    
    let sqlHelper = SQLiteHelper()
    var voteItem: VoteModel = VoteModel()
    var db: OpaquePointer? = nil
    var num: [[Int]] = []
    var num_show: [[Int]] = []
    var recOn: Bool = false
    
    // Audio record
    var recEngine = RecordEngine()
    
    // Color
    let accentColor = UIColor(red: 0, green: 0.4588, blue: 0.8902, alpha: 1.0)
    let recColor = UIColor(red: 0.2, green: 0.6549, blue: 0.2471, alpha: 1.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        for i in 0...voteItem.lastnum {
            num.append([i, 0])
        }
        if sqlHelper.checkDB(recid: voteItem.id) {
            db = sqlHelper.getDB(recid: voteItem.id)
            let orec: [CountModel] = sqlHelper.getRecDB(db: db!)
            for or in orec {
                num[or.vnum][1] += or.vadd
            }
        }
        num_show = num.sorted(by: {$0[1] > $1[1] })
        sTitleLB.text = "\(getTypeTitle(id: voteItem.type))\n\(voteItem.province) เขต \(voteItem.region) หน่วย \(voteItem.unit)"
        if recEngine.engine.isRunning {
            recBut.tintColor = recColor
            recOn = true
        } else {
            recBut.tintColor = UIColor.lightGray
            recOn = false
        }
        recEngine.prepare(vid: voteItem.id)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if voteItem.is_end != 0 {
            recBut.isHidden = true
            manageBut.setTitle("จัดการข้อมูล ›", for: .normal)
        }
        tableView.reloadData()
    }
    
    @IBAction func tapPlus(sender: UIButton) {
        print("plus \(sender.tag)")
        let sc = CountModel()
        sc.id = Int(Date().timeIntervalSince1970)
        sc.vdate = dateToday()
        sc.vadd = 1
        sc.vnum = sender.tag
        if sqlHelper.saveRecData(db: db!, newData: sc) {
            num[sc.vnum][1] += 1
            num_show = num.sorted(by: {$0[1] > $1[1] })
            tableView.reloadData()
        }
    }
    
    @IBAction func tapMinus(sender: UIButton) {
        print("minus \(sender.tag)")
        let sc = CountModel()
        sc.id = Int(Date().timeIntervalSince1970)
        sc.vdate = dateToday()
        sc.vadd = -1
        sc.vnum = sender.tag
        if sqlHelper.saveRecData(db: db!, newData: sc) {
            num[sc.vnum][1] += -1
            num_show = num.sorted(by: {$0[1] > $1[1] })
            tableView.reloadData()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goRecordManage" {
            if let vc = segue.destination as? RecordManage {
                vc.voteItem = voteItem
                vc.recEngine = recEngine
            }
        }
    }
    
    @IBAction func tapEnd() {
        performSegue(withIdentifier: "goRecordManage", sender: nil)
    }
    
    @IBAction func tapRec() {
        if voteItem.is_end == 0 {
            if !recOn {
                recBut.tintColor = recColor
                recEngine.doRec(vid: voteItem.id)
                recOn = true
            } else {
                recBut.tintColor = UIColor.lightGray
                recEngine.stopRec()
                recOn = false
            }
        }
    }
}

extension RecordCount: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return num_show.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CountCell = tableView.dequeueReusableCell(withIdentifier: "countcell", for: indexPath) as! CountCell
        let nshow = num_show[indexPath.row]
        cell.cPlus.tag = nshow[0]
        cell.cMinus.tag = nshow[0]
        if nshow[0] == 0 {
            cell.cNum.text = "บัตร\nเสีย"
            cell.cNum.textColor = UIColor.red
            cell.cNum.layer.borderColor = UIColor.red.cgColor
        } else {
            cell.cNum.text = "\(nshow[0])"
            cell.cNum.textColor = accentColor
            cell.cNum.layer.borderColor = accentColor.cgColor
        }
        cell.cCount.text = "\(nshow[1])"
        
        if voteItem.is_end != 0 {
            cell.cPlus.isHidden = true
            cell.cMinus.isHidden = true
            cell.cCount.textAlignment = .right
            cell.cView.layer.borderColor = UIColor.clear.cgColor
        }
        return cell
    }
}

extension RecordCount: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 88.0
    }
}

extension CALayer {
    @objc var borderUIColor: UIColor {
        set {
            self.borderColor = newValue.cgColor
        }
    
        get {
            return UIColor(cgColor: self.borderColor!)
        }
    }
}
