package com.kaiapp.thaivotecount

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.button.MaterialButton
import java.time.Instant

class RecordCount : Fragment() {

    private var recordEngine = RecordEngine()
    private var adapter: CountAdapter? = null
    private var voteItemID: Int = 0
    var voteItem: VoteModel = VoteModel()
    private var num: MutableList<MutableList<Int>> = ArrayList()
    var numShow: MutableList<MutableList<Int>> = ArrayList()
    private var recOn: Boolean = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.record_count, container, false)
        val db = object : SQLiteHelper(view.context) {}
        voteItemID = arguments?.getInt("voteItemID")!!
        voteItem = db.getMainData(voteItemID)
        val sTitleLB = view.findViewById<TextView>(R.id.sTitleLB)
        sTitleLB.text = voteItem.getTitle()
        val tableView = view.findViewById<RecyclerView>(R.id.tableView)
        tableView.layoutManager = LinearLayoutManager(activity)
        num.clear()
        for (i in 0..voteItem.lastnum) {
            num += mutableListOf(i, 0)
        }
        val counts: Array<CountModel> = db.getRecDB(voteItem.id)
        for (count in counts) {
            num[count.vnum][1] += count.vadd
        }
        sortList()
        val mAdapter: CountAdapter = object : CountAdapter(numShow, activity, voteItem.is_end) {}
        mAdapter.onButtonClick = { cnum, cplus ->
            val sc = CountModel()
            sc.id = Instant.now().epochSecond.toInt()
            sc.vdate = sc.dateToday()
            sc.vadd = cplus
            sc.vnum = cnum
            if (db.saveRecData(voteItem.id, sc)) {
                num[sc.vnum][1] += cplus
                sortList()
                updateTable()
            }
        }
        adapter = mAdapter
        tableView.adapter = mAdapter

        val backBut: Button = view.findViewById(R.id.backBut)
        backBut.setOnClickListener {
            if(recordEngine.state) {
                recordEngine.stopRecording()
            }
            findNavController().popBackStack()
        }
        val recBut: MaterialButton = view.findViewById(R.id.recBut)
        recBut.setOnClickListener {
            if (voteItem.is_end == 0) {
                recOn = if (!recOn) {
                    recBut.setIconTintResource(R.color.green)
                    recordEngine.init(voteItem.id, activity)
                    recordEngine.startRecording()
                    true
                } else {
                    recBut.setIconTintResource(R.color.lightgrey)
                    recordEngine.stopRecording()
                    false
                }
            }
        }
        val manageBut = view.findViewById<Button>(R.id.finBut)
        if(voteItem.is_end > 0) {
            manageBut.text = requireContext().resources.getString(R.string.count_manage)
        } else {
            manageBut.text = requireContext().resources.getString(R.string.count_done)
        }
        manageBut.setOnClickListener {
            if(recordEngine.state) {
                recordEngine.stopRecording()
            }
            val bundle = bundleOf(("voteItemID" to voteItem.id))
            findNavController().navigate(R.id.go_record_manage, bundle)
        }
        return view
    }

    override fun onResume() {
        super.onResume()
        recOn = recordEngine.state
    }

    private fun sortList() {
        numShow = num.sortedWith(compareByDescending { it[1] }).toMutableList()
    }

    @SuppressLint("NotifyDataSetChanged")
    fun updateTable() {
        adapter?.setMList(numShow)
        adapter?.notifyDataSetChanged()
    }
}