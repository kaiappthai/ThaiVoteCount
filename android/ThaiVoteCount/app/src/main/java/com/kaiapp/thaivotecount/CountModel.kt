package com.kaiapp.thaivotecount

import java.text.SimpleDateFormat
import java.util.*

class CountModel {
    var id: Int = 0
    var vnum: Int = 0
    var vadd: Int = 0
    var vdate: String = ""

    fun set(vnum: Int, vadd: Int) {
        this.vnum = vnum
        this.vadd = vadd
        this.vdate = dateToday()
    }

    fun dateToday(): String {
        val destFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
        TimeZone.getTimeZone("Asia/Bangkok").also { destFormat.timeZone = it }
        return destFormat.format(Date())
    }
}