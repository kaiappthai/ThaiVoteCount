package com.kaiapp.thaivotecount

import android.annotation.SuppressLint
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView

abstract class CountAdapter(private var mList: MutableList<MutableList<Int>>, private var mActivity: FragmentActivity?, private var isEnd: Int) : RecyclerView.Adapter<CountAdapter.ViewHolder>() {

    var onButtonClick: ((Int, Int) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.count_cell, parent, false)
        return ViewHolder(view)
    }

    @SuppressLint("RtlHardcoded")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val count = mList[position]
        Log.d("THAIVOTEX", count.toString())
        holder.cCount.text = count[1].toString()
        holder.cPlus.tag = count[0]
        holder.cMinus.tag = count[0]
        if(count[0] == 0) {
            holder.cNum.text = mActivity!!.resources?.getString(R.string.num0)
            holder.cNum.setTextColor(ContextCompat.getColor(mActivity!!, R.color.red))
            holder.cNum.background = ContextCompat.getDrawable(mActivity!!, R.drawable.ic_border3_bg_red)
        } else {
            holder.cNum.text = count[0].toString()
            holder.cNum.setTextColor(ContextCompat.getColor(mActivity!!, R.color.accent))
            holder.cNum.background = ContextCompat.getDrawable(mActivity!!, R.drawable.ic_border3_bg)
        }
        if(isEnd != 0) {
            holder.cPlus.visibility = View.GONE
            holder.cMinus.visibility = View.GONE
            holder.cCount.gravity = Gravity.RIGHT or Gravity.CENTER_VERTICAL
            holder.cCount.background = null
        }
        holder.cPlus.setOnClickListener{
            onButtonClick?.invoke(count[0], 1)
        }
        holder.cMinus.setOnClickListener{
            onButtonClick?.invoke(count[0], -1)
        }
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    fun setMList(nlist:MutableList<MutableList<Int>>) {
        mList = nlist
    }

    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        var cNum: TextView = itemView.findViewById(R.id.cNum)
        var cCount: TextView = itemView.findViewById(R.id.cCount)
        var cPlus: Button = itemView.findViewById(R.id.cPlus)
        var cMinus: Button = itemView.findViewById(R.id.cMinus)
    }
}