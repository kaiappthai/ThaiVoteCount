package com.kaiapp.thaivotecount

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

abstract class VoteAdapter(private var mlist: Array<VoteModel>) : RecyclerView.Adapter<VoteAdapter.ViewHolder>() {

    var onItemClick: ((Int) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.vote_cell, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val voteItem = mlist[position]
        holder.textView.text = voteItem.getTitle()
        Log.d("THAIVOTEX", holder.itemView.toString())
        holder.itemView.setOnClickListener{
            onItemClick?.invoke(position)
        }
    }

    override fun getItemCount(): Int {
        return mlist.size
    }

    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        var textView: TextView = itemView.findViewById(R.id.cNum)
    }
}
