package com.kaiapp.thaivotecount

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class RecordList : Fragment() {

    private var adapter: VoteAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.record_list, container, false)
        val tableView = view.findViewById<RecyclerView>(R.id.tableView)
        tableView.layoutManager = LinearLayoutManager(activity)
        val db = object: SQLiteHelper(view.context) {}
        val votes = db.getMainData()
        val mAdapter: VoteAdapter = object: VoteAdapter(votes) {}
        mAdapter.onItemClick = { position ->
            val bundle = bundleOf(("voteItemID" to votes[position].id))
            findNavController().navigate(R.id.go_record_count, bundle)
        }
        this.adapter = mAdapter

        tableView.adapter = mAdapter

        val addBut = view.findViewById<Button>(R.id.addBut)
        addBut.setOnClickListener {
            findNavController().navigate(R.id.go_create_record)
        }
        val addBut2 = view.findViewById<Button>(R.id.addBut2)
        addBut2.setOnClickListener {
            findNavController().navigate(R.id.go_create_record)
        }
        if(votes.size > 0){
            addBut2.visibility = View.GONE
        } else {
            addBut2.visibility = View.VISIBLE
        }
        return view
    }

    override fun onResume() {
        super.onResume()
        updateTable()
    }

    @SuppressLint("NotifyDataSetChanged")
    fun updateTable() {
        adapter?.notifyDataSetChanged()
    }
}