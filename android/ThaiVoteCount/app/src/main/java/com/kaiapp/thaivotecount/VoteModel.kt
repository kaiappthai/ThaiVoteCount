package com.kaiapp.thaivotecount

import android.util.Log
import org.json.JSONObject

class VoteModel {

    private val typearr = mapOf("1" to "การเลือกตั้งทั่วไป 2566 เขต", "2" to "การเลือกตั้งทั่วไป 2566 บัญชีรายชื่อ")

    var id: Int = 0
    var name: String = ""
    var type: Int = 0
    var province: String = ""
    var region: Int = 0
    var unit: Int = 0
    var lastnum: Int = 0
    var is_end: Int = 0
    var url: String = ""
    var create_date: String = ""
    var end_date: String = ""

    fun getTitle(): String {
        return typearr["$type"] + "\n" + province + " เขต " + region + " หน่วย " + unit
    }

    fun toJsonString(): String {
        val json = JSONObject()
        json.put("id", id)
        json.put("name", name)
        json.put("type", type)
        json.put("province", province)
        json.put("region", region)
        json.put("unit", unit)
        json.put("lastnum", lastnum)
        json.put("is_end", is_end)
        json.put("url", url)
        json.put("create_date", create_date)
        json.put("end_date", end_date)
        return  json.toString()
    }

    fun fromJsonString(jsonStr: String) {
        Log.d("THAIVOTEX", jsonStr)
        val json = JSONObject(jsonStr)
        id = json.getInt("id")
        name = json.getString("name")
        type = json.getInt("type")
        province = json.getString("province")
        region = json.getInt("region")
        unit = json.getInt("unit")
        lastnum = json.getInt("lastnum")
        is_end = json.getInt("is_end")
        url = json.getString("url")
        create_date = json.getString("create_date")
        end_date = json.getString("end_date")
    }
}