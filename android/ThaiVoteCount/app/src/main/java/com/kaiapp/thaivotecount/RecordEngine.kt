package com.kaiapp.thaivotecount

import android.Manifest.permission.RECORD_AUDIO
import android.Manifest.permission.WRITE_EXTERNAL_STORAGE
import android.content.Context
import android.content.pm.PackageManager
import android.media.MediaRecorder
import android.os.Build
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.time.Instant
import java.util.*

class RecordEngine {
    private var context: Context? = null
    private var folder: File? = null
    private var output: String? = null
    private var mediaRecorder: MediaRecorder? = null
    var state: Boolean = false

    fun init(vid: Int, context: FragmentActivity?) {
        this.context = context as Context
        if (ContextCompat.checkSelfPermission(context, RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED &&
            ContextCompat.checkSelfPermission(context, WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            val permissions = arrayOf(RECORD_AUDIO, WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE)
            ActivityCompat.requestPermissions(context, permissions,0)
        } else {
            folder = File(context.filesDir, "$vid")
            if(!folder!!.exists()){
                folder!!.mkdirs()
            }
        }
    }

    fun startRecording() {
        try {
            mediaRecorder = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                MediaRecorder(context!!)
            } else {
                @Suppress("DEPRECATION")
                MediaRecorder()
            }
            mediaRecorder?.setAudioSource(MediaRecorder.AudioSource.MIC)
            mediaRecorder?.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4)
            mediaRecorder?.setAudioEncoder(MediaRecorder.AudioEncoder.AAC)
            mediaRecorder?.setAudioEncodingBitRate(32000)
            mediaRecorder?.setAudioSamplingRate(16000)
            mediaRecorder?.setAudioChannels(1)
            output = File(folder, "audio_" + dateToday() + "_" + Instant.now().epochSecond.toInt() + ".aac").toString()
            mediaRecorder?.setOutputFile(output)
            mediaRecorder?.prepare()
            mediaRecorder?.start()
            state = true
            Toast.makeText(context, "เริ่มบันทึกเสียง", Toast.LENGTH_SHORT).show()
        } catch (e: IllegalStateException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    fun stopRecording(){
        if(state){
            mediaRecorder?.stop()
            mediaRecorder?.reset()
            mediaRecorder?.release()
            mediaRecorder = null
            state = false
            Toast.makeText(context, "จบการบันทึกเสียง", Toast.LENGTH_SHORT).show()
        }
    }

    private fun dateToday(): String {
        val destFormat = SimpleDateFormat("yyyy-MM-dd_HH-mm-ss", Locale.getDefault())
        TimeZone.getTimeZone("Asia/Bangkok").also { destFormat.timeZone = it }
        return destFormat.format(Date())
    }
}