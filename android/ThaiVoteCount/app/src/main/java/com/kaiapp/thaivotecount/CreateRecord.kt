package com.kaiapp.thaivotecount

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Spinner
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import java.text.SimpleDateFormat
import java.time.Instant
import java.util.*

class CreateRecord : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.create_record, container, false)
        val db = object: SQLiteHelper(view.context) {}

        val backBut: Button = view.findViewById(R.id.backBut)
        backBut.setOnClickListener {
            findNavController().popBackStack()
        }
        val nameTF: EditText = view.findViewById(R.id.nameTF)
        val typeButton: Spinner = view.findViewById(R.id.typeButton)
        val provinceButton: Spinner = view.findViewById(R.id.provinceButton)
        val regionTF: EditText = view.findViewById(R.id.regionTF)
        val regionLB: TextView = view.findViewById(R.id.regionLB)
        val unitTF: EditText = view.findViewById(R.id.unitTF)
        val unitLB: TextView = view.findViewById(R.id.unitLB)
        val lastNumButton: Spinner = view.findViewById(R.id.lastNumButton)
        val createBut: Button = view.findViewById(R.id.createBut)
        createBut.setOnClickListener {
            var err = false
            val region = regionTF.text.toString()
            if(region != "" && region != "0") {
                regionLB.visibility = View.INVISIBLE
            } else {
                regionLB.visibility = View.VISIBLE
                err = true
            }
            val unit = unitTF.text.toString()
            if(unit != "" && unit != "0") {
                unitLB.visibility = View.INVISIBLE
            } else {
                unitLB.visibility = View.VISIBLE
                err = true
            }
            if(!err) {
                val voteItem = VoteModel()
                voteItem.id = Instant.now().epochSecond.toInt()
                voteItem.name = nameTF.text.toString()
                voteItem.type = view.resources.getStringArray(R.array.choice_type_value)[typeButton.selectedItemPosition].toInt()
                voteItem.province = provinceButton.selectedItem.toString()
                voteItem.region = region.toInt()
                voteItem.unit = unit.toInt()
                voteItem.lastnum = lastNumButton.selectedItem.toString().toInt()
                voteItem.create_date = dateToday()
                saveVote(db, voteItem)
            }
        }
        return view
    }

    private fun saveVote(db: SQLiteHelper, voteItem: VoteModel) {
        if(db.saveMainData(voteItem)) {
            findNavController().popBackStack()
        }
    }

    private fun dateToday(): String {
        val destFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
        TimeZone.getTimeZone("Asia/Bangkok").also { destFormat.timeZone = it }
        return destFormat.format(Date())
    }
}