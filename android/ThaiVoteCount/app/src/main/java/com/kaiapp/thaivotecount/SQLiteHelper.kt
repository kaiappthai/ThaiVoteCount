package com.kaiapp.thaivotecount

import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import java.sql.SQLException

abstract class SQLiteHelper(context: Context) : SQLiteOpenHelper(context, "thaivote.sqlite", null,
    1) {

    override fun onCreate(db: SQLiteDatabase?) {
        val createTable = "create table if not exists mdata ( id INT PRIMARY KEY NOT NULL, name TEXT, type INT, province TEXT, region INT, unit INT, lastnum INT, is_end INT, url TEXT, create_date TEXT, end_date TEXT)"
        db?.execSQL(createTable)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        //onCreate(db);
    }

    fun saveMainData(newData: VoteModel): Boolean {
        val success: Boolean
        val database = this.writableDatabase
        val contentValues = ContentValues()
        contentValues.put("id", newData.id)
        contentValues.put("name", newData.name)
        contentValues.put("type", newData.type)
        contentValues.put("province", newData.province)
        contentValues.put("region", newData.region)
        contentValues.put("unit", newData.unit)
        contentValues.put("lastnum", newData.lastnum)
        contentValues.put("is_end", newData.is_end)
        contentValues.put("url", newData.url)
        contentValues.put("create_date", newData.create_date)
        contentValues.put("end_date", newData.end_date)
        val result = database.insert("mdata", null, contentValues)
        Log.d("THAIVOTEX", "database.insert = $result")
        success = if (result != (0).toLong()) {
            val createTable =
                "create table if not exists vdata_${newData.id}( id INT PRIMARY KEY NOT NULL, vnum INT, vadd INT, vdate TEXT)"
            Log.d("THAIVOTEX", createTable)
            try {
                database.execSQL(createTable)
                true
            } catch (e: SQLException) {
                Log.d("THAIVOTEX", "saveMainData = $e.message")
                false
            }
        } else {
            false
        }
        database.close()
        Log.d("THAIVOTEX", "saveMainData = $success")
        return success
    }

    fun updateMainData(id: Int, field: String, value: Any): Boolean {
        val database = this.writableDatabase
        val contentValues = ContentValues()
        when (field) {
            "is_end" -> {
                contentValues.put(field, value as Int)
            }
            "end_date" -> {
                contentValues.put(field, value as String)
            }
            "url" -> {
                contentValues.put(field, value as String)
            }
        }
        val result = database.update("mdata", contentValues, "id=?", arrayOf(id.toString()))
        database.close()
        return result != 0
    }

    fun deleteMainData(id: Int): Boolean {
        val database = this.writableDatabase
        val result = database.delete("mdata", "id=?", arrayOf(id.toString()))
        database.close()
        return result != 0
    }

    @SuppressLint("Recycle", "Range")
    fun getMainData(vid: Int): VoteModel {
        val item = VoteModel()
        val database = this.writableDatabase
        val query = "SELECT * FROM mdata WHERE id = $vid"
        val result = database.rawQuery(query, null)
        if (result.moveToFirst()) do {
            item.id = result.getInt(result.getColumnIndex("id"))
            item.name = result.getString(result.getColumnIndex("name"))
            item.type = result.getInt(result.getColumnIndex("type"))
            item.province = result.getString(result.getColumnIndex("province"))
            item.region = result.getInt(result.getColumnIndex("region"))
            item.unit = result.getInt(result.getColumnIndex("unit"))
            item.lastnum = result.getInt(result.getColumnIndex("lastnum"))
            item.is_end = result.getInt(result.getColumnIndex("is_end"))
            item.url = result.getString(result.getColumnIndex("url"))
            item.create_date = result.getString(result.getColumnIndex("create_date"))
            item.end_date = result.getString(result.getColumnIndex("end_date"))
        }
        while (result.moveToNext())
        database.close()
        return  item
    }

    @SuppressLint("Recycle", "Range")
    fun getMainData(): Array<VoteModel> {
        var items: Array<VoteModel> = arrayOf()
        val database = this.writableDatabase
        val query = "SELECT * FROM mdata ORDER BY id DESC"
        val result = database.rawQuery(query, null)
        if (result.moveToFirst()) do {
            val item = VoteModel()
            item.id = result.getInt(result.getColumnIndex("id"))
            item.name = result.getString(result.getColumnIndex("name"))
            item.type = result.getInt(result.getColumnIndex("type"))
            item.province = result.getString(result.getColumnIndex("province"))
            item.region = result.getInt(result.getColumnIndex("region"))
            item.unit = result.getInt(result.getColumnIndex("unit"))
            item.lastnum = result.getInt(result.getColumnIndex("lastnum"))
            item.is_end = result.getInt(result.getColumnIndex("is_end"))
            item.url = result.getString(result.getColumnIndex("url"))
            item.create_date = result.getString(result.getColumnIndex("create_date"))
            item.end_date = result.getString(result.getColumnIndex("end_date"))
            items += item
        }
        while (result.moveToNext())
        database.close()
        return  items
    }

    fun saveRecData(vid: Int, newData: CountModel): Boolean {
        val database = this.writableDatabase
        val contentValues = ContentValues()
        contentValues.put("id", newData.id)
        contentValues.put("vnum", newData.vnum)
        contentValues.put("vadd", newData.vadd)
        contentValues.put("vdate", newData.vdate)
        val result = database.insert("vdata_$vid", null, contentValues)
        database.close()
        return result != (0).toLong()
    }

    @SuppressLint("Recycle", "Range")
    fun getRecDB(vid: Int): Array<CountModel> {
        var items: Array<CountModel> = arrayOf()
        val database = this.writableDatabase
        val query = "SELECT * FROM vdata_$vid ORDER BY id ASC"
        val result = database.rawQuery(query, null)
        if (result.moveToFirst()) do {
            val item = CountModel()
            item.id = result.getInt(result.getColumnIndex("id"))
            item.vnum = result.getInt(result.getColumnIndex("vnum"))
            item.vadd = result.getInt(result.getColumnIndex("vadd"))
            item.vdate = result.getString(result.getColumnIndex("vdate"))
            items += item
        }
        while (result.moveToNext())
        database.close()
        return  items
    }
}
