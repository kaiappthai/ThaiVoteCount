package com.kaiapp.thaivotecount

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import org.json.JSONObject
import java.io.*
import java.net.HttpURLConnection
import java.net.URL
import java.text.SimpleDateFormat
import java.time.Instant
import java.util.*
import java.util.concurrent.Executors


class RecordManage  : Fragment() {

    private var voteItemID: Int = 0
    private var voteItem: VoteModel = VoteModel()
    private var folder: File? = null
    private var allaudio: Array<File> = arrayOf()
    private var allupload: Array<File> = arrayOf()
    private var mView: View? = null
    private var allcount: Array<CountModel> = arrayOf()
    private val uploadURL = "http://localhost/api/votecount/collect"
    private var db: SQLiteHelper? = null

    @SuppressLint("SetTextI18n")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.record_manage, container, false)
        mView = view
        val context = activity as Context
        val db = object : SQLiteHelper(view.context) {}
        this.db = db
        voteItemID = arguments?.getInt("voteItemID")!!
        voteItem = db.getMainData(voteItemID)
        val sTitleLB = view.findViewById<TextView>(R.id.sTitleLB)
        sTitleLB.text = voteItem.getTitle()
        allcount = db.getRecDB(voteItem.id)
        var acno = 0
        for (c in allcount) {
            acno += c.vadd
        }
        val countLB = view.findViewById<TextView>(R.id.countLB)
        countLB.text = "$acno"

        folder = File(requireContext().filesDir, "${voteItem.id}")
        if(!folder!!.exists()){
            folder!!.mkdirs()
        }
        folder!!.walk().forEach {
            if(it.extension == "aac") {
                allaudio += it
            }
            if(it.extension == "json" || it.extension == "csv" || it.extension == "aac") {
                allupload += it
            }
        }
        val audioLB = view.findViewById<TextView>(R.id.audioLB)
        audioLB.text = "${allaudio.size} ไฟล์"
        updateView()

        val backBut: Button = view.findViewById(R.id.backBut)
        backBut.setOnClickListener {
            findNavController().popBackStack()
        }
        val finBut: Button = view.findViewById(R.id.finBut)
        finBut.setOnClickListener {
            voteItem.is_end = 1
            voteItem.end_date = dateToday()
            if(db.updateMainData(voteItem.id, "is_end", voteItem.is_end)) {
                if(db.updateMainData(voteItem.id, "end_date", voteItem.end_date)) {
                    if(saveJsonFile()) {
                        if(saveCSVFile()) {
                            allupload = arrayOf()
                            folder!!.walk().forEach {
                                if(it.extension == "json" || it.extension == "csv" || it.extension == "aac") {
                                    allupload += it
                                }
                            }
                            updateView()
                        }
                    }
                }
            }
        }
        val csvBut: Button = view.findViewById(R.id.csvBut)
        csvBut.setOnClickListener {
            val files = arrayOf(File(folder, "${voteItem.id}.csv"))
            shareFiles(context, files, "text/comma-separated-values")
        }
        val exportBut: Button = view.findViewById(R.id.exportBut)
        exportBut.setOnClickListener {
            var files: Array<File> = arrayOf()
            for (file in allaudio) {
                files += file
            }
            if(files.isNotEmpty()) {
                shareFiles(context, files, "audio/*")
            }
        }
        val onlineBut: Button = view.findViewById(R.id.onlineBut)
        onlineBut.setOnClickListener {
            shareServer()
        }
        val delBut: Button = view.findViewById(R.id.delBut)
        delBut.setOnClickListener {
            if(db.deleteMainData(voteItem.id)) {
                deleteRecursive(folder!!)
                findNavController().popBackStack(R.id.record_list, false)
            }
        }
        return view
    }

    private fun shareFiles(context: Context, files: Array<File>, type: String) {
        try {
            if(files.size == 1){
                val file = files[0]
                if(file.exists()) {
                    val uri = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", file)
                    val intent = Intent(Intent.ACTION_SEND)
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                    intent.type = type
                    intent.putExtra(Intent.EXTRA_STREAM, uri)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                }
            } else if(files.size > 1) {
                var uris: Array<Uri> = arrayOf()
                for (file in files) {
                    if(file.exists()) {
                        uris += FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", file)
                    }
                }
                if(uris.isNotEmpty()) {
                    val intent = Intent(Intent.ACTION_SEND_MULTIPLE)
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                    intent.type = type
                    intent.putExtra(Intent.EXTRA_STREAM, uris)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                }
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }

    private fun getUniqueNumber() = (0..9).shuffled().take(4).joinToString("")

    private fun shareServer() {
        if(this.allupload.isNotEmpty()){
            Executors.newSingleThreadExecutor().execute {
                doSendServer()
            }
        }
    }

    private fun doSendServer() {
        val url = URL(uploadURL)
        val boundary = "Boundary-" + Instant.now().epochSecond.toInt() + getUniqueNumber()

        //Setup the connection
        val connection = url.openConnection() as HttpURLConnection
        val attachmentName = "data"
        connection.requestMethod = "POST"
        connection.connectTimeout = 600000
        connection.useCaches = false
        connection.doOutput = true
        connection.setRequestProperty("Connection", "Keep-Alive")
        connection.setRequestProperty("Cache-Control", "no-cache")
        connection.setRequestProperty("Authorization", "1962cb0881ec5e4c92157ad236b5069f427401498aa8d439ed926a7927074231")
        connection.setRequestProperty("Content-Type", "multipart/form-data;boundary=$boundary")

        //Setup the request
        val request = DataOutputStream(connection.outputStream)

        for (file in this.allupload) {
            Log.d("THAIVOTEX", file.toString())
            request.writeBytes("--$boundary\r\n")
            request.writeBytes("Content-Disposition: form-data; name=\"$attachmentName\";filename=\"${file.name}\"\r\n")
            request.writeBytes("Content-Type: application/octet-stream\r\n\r\n")
            request.write(file.readBytes())
            request.writeBytes("\r\n")
        }
        request.writeBytes("--$boundary--\r\n")
        request.flush()
        request.close()

        //Get response
        val responseStream: InputStream =
            BufferedInputStream(connection.inputStream)

        val responseStreamReader = BufferedReader(InputStreamReader(responseStream))

        var line: String?
        val stringBuilder = StringBuilder()

        while (responseStreamReader.readLine().also { line = it } != null) {
            stringBuilder.append(line).append("\n")
        }
        responseStreamReader.close()

        val response = stringBuilder.toString()
        Log.d("THAIVOTEX", response)
        val json = JSONObject(response)
        if(json.getBoolean("result") && json.getString("url").isNotEmpty()) {
            requireActivity().runOnUiThread {
                voteItem.url = json.getString("url")
                if (this.db!!.updateMainData(voteItem.id, "is_end", voteItem.is_end)) {
                    updateView()
                }
            }
        }
    }

    private fun saveJsonFile(): Boolean {
        val vjson = voteItem.toJsonString()
        val fnpath = File(folder, "${voteItem.id}.json")
        fnpath.writeText(vjson)
        return true
    }

    private fun saveCSVFile(): Boolean {
        var csvtxt = "id,candidate,count,date"
        for (ct in allcount) {
            csvtxt += "\n${ct.id},${ct.vnum},${ct.vadd},${ct.vdate}"
        }
        val fnpath = File(folder, "${voteItem.id}.csv")
        fnpath.writeText(csvtxt)
        return true
    }

    @SuppressLint("SetTextI18n")
    fun updateView() {
        val confirmView: LinearLayout = mView!!.findViewById(R.id.confirmView)
        val statusView: LinearLayout = mView!!.findViewById(R.id.statusView)
        val csvBut: Button = mView!!.findViewById(R.id.csvBut)
        val exportBut: Button = mView!!.findViewById(R.id.exportBut)
        val onlineLB: TextView = mView!!.findViewById(R.id.onlineLB)
        val sendServerView: LinearLayout = mView!!.findViewById(R.id.sendServerView)
        val onlineView: LinearLayout = mView!!.findViewById(R.id.onlineView)
        if(voteItem.is_end == 1) {
            confirmView.visibility = View.GONE
            statusView.visibility = View.VISIBLE
            csvBut.visibility = View.VISIBLE
            exportBut.visibility = View.VISIBLE
            if(allaudio.isNotEmpty()) {
                exportBut.visibility = View.VISIBLE
            } else {
                exportBut.visibility = View.GONE
            }
            if(voteItem.url.isNotEmpty()) {
                onlineLB.text = "ดูการนับคะแนนของคุณได้ที่\n${voteItem.url}"
                sendServerView.visibility = View.GONE
                onlineView.visibility = View.VISIBLE
                onlineView.setOnClickListener {
                    val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(voteItem.url))
                    startActivity(browserIntent)
                }
            } else {
                sendServerView.visibility = View.VISIBLE
                onlineView.visibility = View.GONE
            }
        } else {
            confirmView.visibility = View.VISIBLE
            statusView.visibility = View.GONE
            csvBut.visibility = View.GONE
            exportBut.visibility = View.GONE
            sendServerView.visibility = View.GONE
            onlineView.visibility = View.GONE
        }
    }

    private fun dateToday(): String {
        val destFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
        TimeZone.getTimeZone("Asia/Bangkok").also { destFormat.timeZone = it }
        return destFormat.format(Date())
    }

    private fun deleteRecursive(fileOrDirectory: File) {
        if (fileOrDirectory.isDirectory) for (child in fileOrDirectory.listFiles()!!) deleteRecursive(
            child
        )
        fileOrDirectory.delete()
    }
}
